%% Stores variables and constants to define scenario
scen.name = "Scenario 1";
scen.t_end = 800;    % How long to run scenario
scen.dt = 1;         % Scenario run frequency
scen.USE_SIMULINK = 0;

%% Define target parameters
% Target state = x pos, x speed, y pos, y speed
scen.Y0 = [120; -0.3867; 60; 0];    % Initial target state
% Target dynamic function
scen.target_dyn = @(T, Y) target_dynamics(T, Y, accel_profile(T, Y));
scen.turn = @(t) turn_seq(t);
scen.accel = @(x) accel_profile(x(1), [x(2); x(3); x(4); x(5)]);
scen.turns = [250 350 575 675];

%% Define Radar Parameters
scen.dt_m = 5;   % Radar update rate
scen.sig_m = 0.1;     % Radar measurement noise
scen.H = [1 0 0 0; 0 0 1 0];    % Radar measurement matrix
scen.F = [1 scen.dt 0 0;
          0 1 0 0;
          0 0 1 scen.dt;
          0 0 0 1]; % Constant velocity interpolation between updates
scen.Fm = [1    scen.dt_m   0       0;
           0    1           0       0;
           0    0           1       scen.dt_m;
           0    0           0       1]; % Measurement interpolation
scen.G = [0.5*scen.dt^2 0;
          scen.dt       0
          0             0.5*scen.dt^2;
          0             scen.dt]; % Control (acceleration) matrix
scen.K0 = [1 0;
            0 0;
            0 1;
            0 0];
scen.K1 = [1 0;
            1/scen.dt 0;
            0 1;
            0 1/scen.dt];
scen.sig_v = 0.003382968291111;     % Process noise (target accel)
                                    % Calcualted from
                                    % Estimate_Process_Noise
scen.sig_v_rand = [0; 0];
scen.TI = (scen.sig_v/scen.sig_m)*scen.dt_m^2;   % Tracking index for constant filter gain
scen.alpha = (-1/8)*(scen.TI^2 + 8*scen.TI - ...
    (scen.TI + 4) * sqrt(scen.TI^2 + 8*scen.TI));   % Alpha for constant filter gain
scen.beta = 2*(2 - scen.alpha) - 4*sqrt(1 - scen.alpha);    % Beta for constant filter gain
scen.K = [scen.alpha 0;
          scen.beta/scen.dt 0;
          0 scen.alpha;
          0 scen.beta/scen.dt];

%% Target Function definitions
% Define target turn sequence
function w = turn_seq(t)
    if t < 250
        w = 0;
    elseif t <= 350
        w = deg2rad(1);
    elseif t < 575
        w = 0;
    elseif t <= 675
        w = -deg2rad(1);
    else
        w = 0;
    end
end

% Convert turn sequence to normal acceleration
function u = accel_profile(t, y)
    speed = norm([y(2) y(4)]);
    w = turn_seq(t);
    u = [0; speed*w];
end


