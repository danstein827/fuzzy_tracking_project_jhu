% Fuzzy Inference Engine for adaptive alpha-beta filter

function dTI = mod_alpha_beta_FIE(r, sig_m, nMeas)
    persistent last_ek R
    
    if (isempty(last_ek) || (nMeas == 1))
        last_ek = 0;
        R = 0;
    end
    
    % Residual recursive filter
    R = (R + r/(sig_m)) / 2;
    
    % Get error and error change
    ek = sqrt(sum(R.^2)) / 2;
    dek = ek - last_ek;
        
    
    % Membership functions for ek
    mf_e = [lTri_MF(ek, 0, 0.6) Trap_MF(ek, 0.2, 0.6, 0.8, 2.2) rTri_MF(ek, 0.8, 2.2)];
    
    % Membership function for dek
    mf_de = [lTri_MF(dek, -1.5, 0);
            Tri_MF(dek, -1.5, 0, 1.5);
            rTri_MF(dek, 0, 1.5)];

    % Fuzzy relationship table
    rel = [-0.5 -0.5 -0.5;
           0 -0.5 0.5;
           0.5 -0.5 0.5];
       
    % Defuzzify
    dTI = sum(rel.*mf_e.*mf_de, 'all')/sum(mf_e.*mf_de, 'all');
    
    last_ek = ek;
end

%% Membership Functions
function u = lTri_MF(x, b, c)
    if x <= b
        u = 1;
    elseif x < c
        u = (c - x) / (c - b);
    else
        u = 0;
    end
end

function u = rTri_MF(x, a, b)
    if x <= a
        u = 0;
    elseif x < b
        u = (x - a) / (b - a);
    else
        u = 1;
    end
end

function u = Tri_MF(x, a, b, c)
    if x <= a
        u = 0;
    elseif x < b
        u = (x - a) / (b - a);
    elseif x < c
        u = (c - x) / (c - b);
    else
        u = 0;
    end
end

function u = Trap_MF(x, a, b, c, d)
    if x <= a
        u = 0;
    elseif x < b
        u = (x - a) / (b - a);
    elseif x <= c
        u = 1;
    elseif x < d
        u = (d - x) / (d - c);
    else
        u = 0;
    end
end