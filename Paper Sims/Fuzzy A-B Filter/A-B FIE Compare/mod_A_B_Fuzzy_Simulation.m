% Simulates tracking a target using the fuzzy alpha-beta tracker


% Check if seed has been created, for when not running via monte-carlo
if not(exist('seed'))
    seed = 1234632;
end
rng(seed);

% Check if a simulation file as been loaded
if not(exist('scen'))
    Scenario1
end

% Setup time variable
t = 0:scen.dt:scen.t_end;

% Run target simulation
if not(exist('y'))
    [t, y] = ode113(scen.target_dyn, t, scen.Y0);
end

% Initial estimate is blank
y_hat = [0; 0; 0; 0];
TI = 1;   % Initial tracking index
TI_min = 0.02;  % Minimum tracking index. Cannot allow 0 or negative
alpha = 0;      % Initial alpha value
beta = 0;       % Initial beta value
n_meas = 0;     % Number of radar measurements taken
next_t_measure = 0; % Time the next measurement should be taken

% Initialize history arrays
y_hat_hist = zeros(size(y));
k_hist = zeros([0 3]);
meas_hist = zeros([0 15]);

% Run the time simulation
for i = 1:length(t)
    % Check if it is time to take a radar measurement
    if t(i) >= next_t_measure
        n_meas = n_meas + 1;    % Increment number of measurements
        next_t_measure = next_t_measure + scen.dt_m; % Schedule next measurement
        % Take a measurement
        measure = scen.H*y(i,:)' + scen.sig_m*randn([2 1]);
        r = measure - scen.H*y_hat;      % Measurement to estimate error
        
        if n_meas == 1
            % Cannot estimate based on first measure
            k = scen.K0;
        elseif n_meas == 2
            % Cannot estimate velocity based on second measure
            k = scen.K1;
        else
            % Use FIE to adjust tracking index
            TI = max([TI + mod_alpha_beta_FIE(r, scen.sig_m, n_meas - 2); TI_min]);
            % Calculate alpha and beta coeffiecents based on adjusted
            % tracking index
            alpha = (-1/8)*(TI^2 + 8*TI - ...
                (TI + 4) * sqrt(TI^2 + 8*TI));
            beta = 2*(2 - alpha) - 4*sqrt(1 - alpha);
            % Return alpha-beta gain matrix
            k = [alpha 0;
                beta/scen.dt_m 0;
                0 alpha;
                0 beta/scen.dt_m];
        end
        
        k_hist(end+1,:) = [TI alpha beta];
        % Measure History
        % C 1 - Time
        % C 2-3 - Radar measurement at t
        % C 4-7 - Target actual postion
        % C 8-11 - Target estimate pre-adjustment
        % C 12-15 - Target estimate post-adjustment
        meas_hist(end+1, 1:11) = [t(i) measure' y(i,:) (y_hat)'];
        
        % Update prediction based on measurement
        y_hat = y_hat + k*r;
        meas_hist(end, 12:15) = (y_hat)';
        
    end
    
    y_hat_hist(i,:) = y_hat';
    
    % Update target predictions
    y_hat = scen.F * y_hat;
end