%% Implements Curvilinear Dynamics model
% Returns target state derivative from passed time, state, and control
% input.
% Control input is 2-vector containing tangential and perpendicular
% acceleration

function dy = target_dynamics(t, y, u)
    % State matrix
    A = [0 1 0 0; 
        0 0 0 0; 
        0 0 0 1; 
        0 0 0 0];
    
    % Get the velocity direction unit vector
    vel = [y(2) y(4)] / norm([y(2) y(4)]);
    
    B = [0 0;
        vel(1) -vel(2);
        0 0;
        vel(2) vel(1)];
    
    dy = A*y + B*u;
end