% Runs Monte-Carlo of the fuzzy alpha-beta tracking system

% Clear variables and plots
% clear; close all; clc;

% Load scenario
if not(exist('scen'))
    Scenario1
end

if not(exist('N'))
    N = 10000;      % Number of trials to run
end

if not(exist('baseSeed'))
    baseSeed = 562154;
end

% Generate a sequence of seeds
rng(baseSeed)
seeds = round(10000*rand([1 N]));
t_measure = 0:scen.dt_m:scen.t_end;

pos_Error = zeros([N length(t_measure)]);
vel_Error = zeros([N length(t_measure)]);
tracking_Index_RMS = zeros(size(t_measure'));

for trial = 1:N
    seed = seeds(trial);

    % Run simulation
    mod_A_B_Fuzzy_Simulation;
    
    pos_Error(trial,:) = ((meas_hist(:,4) - meas_hist(:,12)).^2 + ...
        (meas_hist(:,6) - meas_hist(:,14)).^2)';
    vel_Error(trial,:) = ((meas_hist(:,5) - meas_hist(:,13)).^2 + ...
        (meas_hist(:,7) - meas_hist(:,15)).^2)';
    tracking_Index_RMS = tracking_Index_RMS + (k_hist(:, 1).^2) / N;
    
    if (mod(trial, 500) == 0)
        disp("Trial Number: " + trial)
    end
end

pos_Error_RMS = sqrt(sum(pos_Error/N, 1));
vel_Error_RMS = sqrt(sum(vel_Error/N, 1));

pos_Error_STD = (sum(pos_Error - pos_Error_RMS, 1)/(N-1)).^2;
vel_Error_STD = (sum(vel_Error - vel_Error_RMS, 1)/(N-1)).^2;
tracking_Index_RMS = sqrt(tracking_Index_RMS);
