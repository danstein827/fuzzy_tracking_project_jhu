% Compares FIEs

clear; % clc; % close all

Scenario1;

A_B_Fuzzy_Monte_Carlo;

STD.Pos_RMS = pos_Error_RMS;
STD.Vel_RMS = vel_Error_RMS;
STD.TI = tracking_Index_RMS;

mod_A_B_Fuzzy_Monte_Carlo;

MOD.Pos_RMS = pos_Error_RMS;
MOD.Vel_RMS = vel_Error_RMS;
MOD.TI = tracking_Index_RMS;

[mean(STD.Pos_RMS) mean(MOD.Pos_RMS); mean(STD.Vel_RMS) mean(MOD.Vel_RMS)]

figure()
plot(meas_hist(30:end, 1), STD.Pos_RMS(30:end))
hold on
plot(meas_hist(30:end, 1), MOD.Pos_RMS(30:end))
for i = 1:length(scen.turns)
    xline(scen.turns(i), 'k--')
end
hold off
grid on; grid minor
xlabel("Time (s)"); ylabel("Error RMS"); title(scen.name + " Position Error")
legend(["Original", "Modified"], 'Location', 'best')


figure()
plot(meas_hist(30:end, 1), STD.Vel_RMS(30:end))
hold on
plot(meas_hist(30:end, 1), MOD.Vel_RMS(30:end))
for i = 1:length(scen.turns)
    xline(scen.turns(i), 'k--')
end
hold off
grid on; grid minor
xlabel("Time (s)"); ylabel("Error RMS"); title(scen.name + " Velocity Error")
legend(["Original", "Modified"], 'Location', 'best')

figure()
plot(meas_hist(30:end, 1), STD.TI(30:end))
hold on
plot(meas_hist(30:end, 1), MOD.TI(30:end))
for i = 1:length(scen.turns)
    xline(scen.turns(i), 'k--')
end
hold off
grid on; grid minor
xlabel("Time (s)"); ylabel("\lambda"); title(scen.name + " Tracking Index")
legend(["Original", "Modified"], 'Location', 'best')