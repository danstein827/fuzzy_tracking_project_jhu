% Plots Alpha-Beta FIE Fuzzy Sets

e = 0:0.1:3;
de = -3:0.1:3;

mf_e = [];
mf_de = [];
for ek = e
    mf_e(:,end+1) = [lTri_MF(ek, 0, 0.6) Trap_MF(ek, 0.2, 0.6, 0.8, 2.2) rTri_MF(ek, 0.8, 2.2)]';
end

for dek = de
    mf_de(:,end+1) = [lTri_MF(dek, -1.5, 0);
                Tri_MF(dek, -1.5, 0, 1.5);
                rTri_MF(dek, 0, 1.5)];
end

figure()
subplot(211)
plot(e, mf_e)
legend(["ZE", "MP", "LP"], 'Location', 'best');
xlabel("e")
ylabel("Membership")
ylim([0 1.1])

subplot(212)
plot(de, mf_de)
legend(["NE", "ZE", "EP"], 'Location', 'best');
xlabel("\Deltae")
ylabel("Membership")
ylim([0 1.1])


function u = lTri_MF(x, b, c)
    if x <= b
        u = 1;
    elseif x < c
        u = (c - x) / (c - b);
    else
        u = 0;
    end
end

function u = rTri_MF(x, a, b)
    if x <= a
        u = 0;
    elseif x < b
        u = (x - a) / (b - a);
    else
        u = 1;
    end
end

function u = Tri_MF(x, a, b, c)
    if x <= a
        u = 0;
    elseif x < b
        u = (x - a) / (b - a);
    elseif x < c
        u = (c - x) / (c - b);
    else
        u = 0;
    end
end

function u = Trap_MF(x, a, b, c, d)
    if x <= a
        u = 0;
    elseif x < b
        u = (x - a) / (b - a);
    elseif x <= c
        u = 1;
    elseif x < d
        u = (d - x) / (d - c);
    else
        u = 0;
    end
end