TI = 0:0.01:10;

alpha = (-1/8)*(TI.^2 + 8*TI - (TI + 4).*sqrt(TI.^2 + 8*TI));
beta = 2*(2 - alpha) - 4*sqrt(1-alpha);

plot(TI, [alpha; beta])