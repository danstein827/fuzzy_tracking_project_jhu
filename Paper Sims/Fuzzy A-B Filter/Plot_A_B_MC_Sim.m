% Plot results from fuzzy alpha-beta monte-carlo runs
% Plots positonal and velocity error RMS and tracking index RMS

figure()
plot(meas_hist(30:end,1), pos_Error_RMS(30:end))
for i = 1:length(scen.turns)
    xline(scen.turns(i), 'k--')
end
xlabel("Time (s)"); ylabel("Position Error RMS (km)")
title(scen.name + " Pos RMS")
grid on; grid minor

figure()
plot(meas_hist(30:end,1), vel_Error_RMS(30:end))
xlabel("Time (s)"); ylabel("Velocity Error RMS (km/s)")
for i = 1:length(scen.turns)
    xline(scen.turns(i), 'k--')
end
title(scen.name + " Vel RMS")
grid on; grid minor

figure()
plot(meas_hist(30:end,1), tracking_Index_RMS(30:end))
for i = 1:length(scen.turns)
    xline(scen.turns(i), 'k--')
end
xlabel("Time (s)"); ylabel("Tracking Index \lambda")
title(scen.name + " \lambda")
grid on; grid minor