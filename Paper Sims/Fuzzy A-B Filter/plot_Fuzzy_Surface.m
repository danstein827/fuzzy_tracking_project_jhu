% Use the alpha-beta FIE to plot the output surface

E = 0:0.25:3;
DE = -3:0.25:3;

fuzzy_suf = zeros([length(E) length(DE)]);

for e = 1:length(E)
    for de = 1:length(DE)
        fuzzy_suf(e, de) = alpha_beta_FIE_test(E(e), DE(de));
    end
end

surf(E, DE, fuzzy_suf')
xlim([0 3])
ylim([-3 3])
zlim([-0.5 0.5])
xlabel("e"); ylabel("\Delta e"); zlabel("\Delta \lambda")

