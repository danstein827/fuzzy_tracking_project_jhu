function dq = Q_FIE(dom)
    m = [ls_gaussian_MF(dom, -0.4, 0.05);
        gaussian_MF(dom, -0.2, 0.05);
        gaussian_MF(dom, -0.05, 0.05)
        gaussian_MF(dom, 0, 0.05);
        gaussian_MF(dom, 0.05, 0.05)
        gaussian_MF(dom, 0.2, 0.05);
        rs_gaussian_MF(dom, 0.4, 0.05)];
    
    % CA Defuzz
    c = [.5 .2 0.1 0 -0.1 -.2 -.5];
    
    dq = c*m/sum(m);

end