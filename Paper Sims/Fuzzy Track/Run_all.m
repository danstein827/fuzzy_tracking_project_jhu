plotter = 0;

Kalman_Opt
opt.J1 = J1;
opt.J2 = J2;

Kalman_Degraded
deg.J1 = J1;
deg.J2 = J2;

Fuzzy_Kalman
fuz.J1 = J1;
fuz.J2 = J2;

[opt.J1 deg.J1 fuz.J1;
    opt.J2 deg.J2 fuz.J2]

figure()
plot(t(1:end-1), est_hist)
yline(sig_v, 'k--')
legend(["\omega_p est", "\omega_p"])
title("\omega_p Fuzzy Estimation")
xlabel("Time (s)"); ylabel("\omega_p")