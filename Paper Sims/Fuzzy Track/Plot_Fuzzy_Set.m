% Plots the Fuzzy Sets

DOM = -0.4:0.01:0.4;

m = [];

for dom = DOM
    m(:,end+1) = [ls_gaussian_MF(dom, -0.4, 0.05);
        gaussian_MF(dom, -0.2, 0.05);
        gaussian_MF(dom, -0.05, 0.05)
        gaussian_MF(dom, 0, 0.05);
        gaussian_MF(dom, 0.05, 0.05)
        gaussian_MF(dom, 0.2, 0.05);
        rs_gaussian_MF(dom, 0.4, 0.05)];
end

DQ = [-.5:0.01:0.5];

m_out = [];
for dq = DQ
    m_out(:, end+1) = [Tri_MF(dq, 0.2, 0.5, 0.7);
        Tri_MF(dq, 0.1, 0.2, 0.5);
        Tri_MF(dq, 0, 0.1, 0.2);
        Tri_MF(dq, -0.1, 0, 0.1);
        Tri_MF(dq, -0.2, -0.1, 0);
        Tri_MF(dq, -0.5, -0.2, -0.1);
        Tri_MF(dq, -0.7, -0.5, -0.2)];
end

DQ = 100*DQ;


subplot(211)
plot(DOM, m)
xlabel("Degree of Membership"); ylabel("Membership")
legend(["NL", "NM", "NS", "ZE", "PS", "PM", "PL"], 'Location', 'best')

subplot(212)
plot(DQ, m_out)
xlabel("% \Delta Q"); ylabel("Membership")
legend(["IL", "I", "IS", "M", "DS", "D", "DL"], 'Location', 'best')

function u = Tri_MF(x, a, b, c)
    if x <= a
        u = 0;
    elseif x < b
        u = (x - a) / (b - a);
    elseif x < c
        u = (c - x) / (c - b);
    else
        u = 0;
    end
end