Tracking_Test_Setup

t = 0:dt:t_max;
tm = 0:dtm:t_max;

y_hat = zeros(size(Y0));
y = Y0;

J1 = [0];
J2 = [0; 0];

R = R_opt;
Q = G*diag((10*sig_v).^2)*G';

t_next_meas = 0;
n_meas = 0;

y_hist = zeros([length(t) 2]);
y_hat_hist = zeros([length(t) 2]);

for i = 1:length(t)
    if t(i) >= t_next_meas
        n_meas = n_meas + 1;
        t_next_meas = t_next_meas + dtm;
        % Take Measurement
        me = sig_m .* randn([1 1]);
        measure = H*y + me;
        r = measure - H*y_hat;

        % Calculate Kalman Gain
        S = H*P*H' + R;
        K = P*H'/S;

        % Update Estiamte
        y_hat = y_hat + K*r;
        P = (eye(2) - K*H)*P;

        % Update Performance measure
        J1 = J1 + (me.^2)/length(tm);
        J2 = J2 + ((y - y_hat).^2)/length(tm);
    end

    % Update histories
    y_hist(i,:) = y';
    y_hat_hist(i,:) = y_hat';
    
    
    % Update States
    pe = G*sig_v .* randn([1 1]);
    y = F*y + pe;
    
    y_hat = F*y_hat;
    P = F*P*F' + Q; 
end

J1 = sqrt(J1);
J2 = sqrt(J2);

if plotter == 1
    subplot(211)
    plot(t, y_hist(:,1))
    hold on
    plot(t, y_hat_hist(:,1))
    legend(["Act", "Est"])
    
    subplot(212)
    plot(t, y_hist(:,1))
    hold on
    plot(t, y_hat_hist(:,1))
end