function u = ls_gaussian_MF(X, c, s)
    u = ones(size(X(X < c)));
    u = [u exp(-0.5*((X(X >= c) - c)/s).^2)];
end