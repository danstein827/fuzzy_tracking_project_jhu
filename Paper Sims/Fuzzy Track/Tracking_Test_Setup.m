if not(exist('seed'))
    rng(56853215)
else
    rng(seed)
end

if not(exist('plotter'))
    plotter = 0;
end

dt = 0.2;
dtm = 0.2;
t_max = 500;

F = [1  dt;
    0   1];

Fm = [1     dtm;
      0     1];
    
G = [0.5*dt^2;
    dt];

Gm = [0.5*dtm^2;
    dtm];

H = [1 0];

sig_m = 0.1;
sig_v = 0.1;

Y0 = rand([2 1]).*[100; 100] - [0; 50];

Q_opt = G*diag(sig_v.^2)*G';
R_opt = diag(sig_m.^2);

P = 100*eye(2);