Tracking_Test_Setup

t = 0:dt:t_max;
tm = 0:dtm:t_max;

y_hat = zeros(size(Y0));
y = Y0;

J1 = [0];
J2 = [0; 0];

R = R_opt;
sig_est = 10*sig_v;
Q = G*diag(sig_est.^2)*G';

Nr = 30;
laglen = 2*Nr;
r_buffer = zeros([Nr 1]);
t_next_meas = 0;
n_meas = 0;

y_hist = zeros([length(t) 2]);
y_hat_hist = zeros([length(t) 2]);
est_hist = [];

for i = 1:length(t)
    if t(i) >= t_next_meas
        n_meas = n_meas + 1;
        t_next_meas = t_next_meas + dtm;
        
        % Take Measurement
        me = sig_m .* randn([1 1]);
        measure = H*y + me;
        r = measure - H*y_hat;

        % Calculate Kalman Gain
        S = H*P*H' + R;
        K = P*H'/S;

        % Update Estiamte
        y_hat = y_hat + K*r;
        P = (eye(2) - K*H)*P;

        % Store residual to buffer
        r_buffer(mod(n_meas-1, Nr)+1,:) = r';

        % Calculate residual covarience
        C = 0;
        for j = 1:Nr
            C = C + (r_buffer(j,:)'*r_buffer(j,:))/Nr;
        end

        % Recalculate covarience
        S = H*(Fm*P*Fm' + Q)*H' + R;
        DoM = (S - C);    % Degree of Measure
        dsig = [Q_FIE(DoM)];

        lag = exp(-(laglen - min(n_meas, laglen))/10);
        dsig = 1 + lag*(dsig);
        sig_est = (sig_est*dsig);
        Q = G*diag(sig_est.^2)*G';
        
        est_hist(n_meas) = sig_est;

        % Update Performance measure
        J1 = J1 + (me.^2)/length(tm);
        J2 = J2 + ((y - y_hat).^2)/length(tm);
    end
    
    % Update histories
    y_hist(i,:) = y';
    y_hat_hist(i,:) = y_hat';

    
    % Update States
    pe = G*(sig_v) .* randn([1 1]);
    y = F*y + pe;
    
    y_hat = F*y_hat;
    P = F*P*F' + Q; 
end

J1 = sqrt(J1);
J2 = sqrt(J2);

if plotter == 1
    subplot(211)
    plot(t, y_hist(:,1))
    hold on
    plot(t, y_hat_hist(:,1))
    legend(["Act", "Est"])
    
    subplot(212)
    plot(t, y_hist(:,1))
    hold on
    plot(t, y_hat_hist(:,1))
end
    