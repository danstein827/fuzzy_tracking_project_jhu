% Runs the test with an optimal Kalman filter for baseline comparison

Fuzzy_Kalman_Setup

t = 0:dt:t_max;

y_hat = [0; 0; 0];
y = Y0;

J1 = [0; 0];
J2 = [0; 0; 0];

R = R_opt;
Q = Q_opt;

y_hist = zeros([length(t) 3]);
y_hat_hist = zeros([length(t) 3]);

for i = 1:length(t)
    % Take Measurement
    me = sig_m .* randn([2 1]);
    measure = H*y + me;
    r = measure - H*y_hat;
    
    % Calculate Kalman Gain
    S = H*P*H' + R;
    K = P*H'/S;
    
    % Update Estiamte
    y_hat = y_hat + K*r;
    P = (eye(3) - K*H)*P;
    
    % Update Performance measure
    J1 = J1 + (me.^2)/length(t);
    J2 = J2 + ((y - y_hat).^2)/length(t);
    
    % Update histories
    y_hist(i,:) = y';
    y_hat_hist(i,:) = y_hat';
    
    % Update States
    pe = sig_v .* randn([3 1]);
    y = F*y + pe;
    
    y_hat = F*y_hat;
    P = F*P*F' + Q; 
end

J1 = sqrt(J1);
J2 = sqrt(J2);

if plotter == 1
    subplot(211)
    plot(t, y_hist(:,1))
    hold on
    plot(t, y_hat_hist(:,1))
    legend(["Act", "Est"])
    
    subplot(212)
    plot(t, y_hist(:,1))
    hold on
    plot(t, y_hat_hist(:,1))
end