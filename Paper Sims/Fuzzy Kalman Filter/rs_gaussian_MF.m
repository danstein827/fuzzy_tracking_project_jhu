function u = rs_gaussian_MF(X, c, s)
    u = exp(-0.5*((X(X <= c) - c)/s).^2);
    u = [u ones(size(X(X > c)))];
end