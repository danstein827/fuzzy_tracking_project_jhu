% Sets initial variables for fuzzy Kalman filter test
% Values taken from DEVELOPMENT OF A FUZZY LOGIC-BASED ADAPTIVE KALMAN
% FILTER, Escamilla-Ambrosio, Mort

rng(85621125)

dt = 0.5;
t_max = 500;

F = [0.77 0.2 0;
    0.25 0.75 0.25;
    0.05 0 0.75];
G = eye(3);
H = [1 0 0;
    0 1 0];

sig_m = sqrt([0.2 15])';
sig_v = sqrt(0.02)*[1 1 1]';

Y0 = rand([3 1]).*[100; 100; 0] - [0; 50; 0];

Q_opt = diag(sig_v.^2);
R_opt = diag(sig_m.^2);

P = 100*eye(3);