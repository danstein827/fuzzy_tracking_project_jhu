% Plots the Fuzzy Sets

DOM = -0.3:0.01:0.3;

m = [];

for dom = DOM
    m(:,end+1) = [ls_gaussian_MF(dom, -0.3, 0.05);
        gaussian_MF(dom, -0.15, 0.05);
        gaussian_MF(dom, 0, 0.05);
        gaussian_MF(dom, 0.15, 0.05);
        rs_gaussian_MF(dom, 0.3, 0.05)];
end

DQ = [-2:0.1:2];

m_out = [];

for dq = DQ
    m_out(:, end+1) = [Tri_MF(dq, 1, 2, 3);
        Tri_MF(dq, 0, 1, 2);
        Tri_MF(dq, -1, 0, 1);
        Tri_MF(dq, -2, -1, 0);
        Tri_MF(dq, -3, -2, -1)];
end

DQ = DQ*1e-3;

subplot(211)
plot(DOM, m)
xlabel("Degree of Membership"); ylabel("Membership")
legend(["NM", "NS", "ZE", "PS", "PM"], 'Location', 'best')

subplot(212)
plot(DQ, m_out)
xlabel("\Delta Q"); ylabel("Membership")
legend(["IL", "I", "M", "D", "DL"], 'Location', 'best')

function u = Tri_MF(x, a, b, c)
    if x <= a
        u = 0;
    elseif x < b
        u = (x - a) / (b - a);
    elseif x < c
        u = (c - x) / (c - b);
    else
        u = 0;
    end
end