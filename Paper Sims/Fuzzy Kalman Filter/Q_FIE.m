function dq = Q_FIE(dom)
    m = [ls_gaussian_MF(dom, -0.3, 0.05);
        gaussian_MF(dom, -0.15, 0.05);
        gaussian_MF(dom, 0, 0.05);
        gaussian_MF(dom, 0.15, 0.05);
        rs_gaussian_MF(dom, 0.3, 0.05)];
    
    % CA Defuzz
    c = [2 1 0 -1 -2]*10^-3;
    
    dq = c*m/sum(m);

end