% Plot simulation
figure()
plot(y(:,1), y(:,3), 'DisplayName', 'Target');
hold on
plot(y_hat_hist(:,1), y_hat_hist(:,3), 'r', 'DisplayName', 'Track')
plot(y(1,1), y(1,3), 'go', 'DisplayName', 'Start')
plot(y(end,1), y(end,3), 'gs', 'DisplayName', 'end')
plot(0,0, 'r+', 'DisplayName', 'Radar')
legend('show', 'Location', 'best')
hold off
grid on; grid minor;
xlabel('X (km)')
ylabel('Y (km)')
title(scen.name)