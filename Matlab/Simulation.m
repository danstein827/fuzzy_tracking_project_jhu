Scenario2
rng(1234632);

t = 0:scen.dt:scen.t_end;
[t, y] = ode113(scen.target_dyn, t, scen.Y0);

y_hat = [0; 0; 0; 0];
TI = 0.1;   % Initial tracking index
TI_min = 0.10;
alpha = 0;
beta = 0;
n_meas = 0;
next_t_measure = 0;

y_hat_hist = zeros(size(y));
meas_hist = zeros([0 15]);
for i = 1:length(t)
    if t(i) >= next_t_measure
        n_meas = n_meas + 1;    % Increment number of measurements
        next_t_measure = next_t_measure + scen.dt_m;
        % Take a measurement
        measure = scen.H*y(i,:)' + scen.sig_m*randn([2 1]);
        e = measure - scen.H*y_hat;      % Measurement to estimate error
        
        if n_meas == 1
            % Cannot estimate based on first measure
            k = scen.K0;
        elseif n_meas == 2
            % Cannot estimate velocity based on second measure
            k = scen.K1;
        else 
            k = scen.K;
        end
        
        
        meas_hist(end+1, 1:11) = [t(i) measure' y(i,:) (y_hat)'];
        
        % Update prediction based on measurement
        y_hat = y_hat + k*e;
        meas_hist(end, 12:15) = (y_hat)';
        
    end
    
    y_hat_hist(i,:) = y_hat';
    
    % Update target predictions
    y_hat = scen.G * y_hat;
end