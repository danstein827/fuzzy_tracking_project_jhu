%% Stores variables and constants to define scenario
scen.name = "Scenario 3";
scen.t_end = 800;    % How long to run scenario
scen.dt = 1;         % Scenario run frequency
scen.USE_SIMULINK = 0;

%% Define target parameters
% Target state = x pos, x speed, y pos, y speed
scen.Y0 = [120; -0.368; 60; 0];    % Initial target state
% Target dynamic function
scen.target_dyn = @(T, Y) target_dynamics(T, Y, accel_profile(T, Y));
scen.turn = @(t) turn_seq(t);
scen.accel = @(x) accel_profile(x(1), [x(2); x(3); x(4); x(5)]);
scen.turns = [150 350 550 700 775];

%% Define Radar Parameters
scen.dt_m = 5;   % Radar update rate
scen.sig_m = 0.1;     % Radar measurement noise
scen.H = [1 0 0 0; 0 0 1 0];    % Radar measurement matrix
scen.F = [1 scen.dt 0 0;
          0 1 0 0;
          0 0 1 scen.dt;
          0 0 0 1]; % Constant velocity interpolation between updates
scen.Fm = [1    scen.dt_m   0       0;
           0    1           0       0;
           0    0           1       scen.dt_m;
           0    0           0       1]; % Measurement interpolation
scen.G = [0.5*scen.dt^2 0;
          scen.dt       0
          0             0.5*scen.dt^2;
          0             scen.dt]; % Control (acceleration) matrix
scen.K0 = [1 0;
            0 0;
            0 1;
            0 0];
scen.K1 = [1 0;
            1/scen.dt 0;
            0 1;
            0 1/scen.dt];
scen.sig_v = 0.0072;     % Process noise (target accel)
                                    % Calcualted from
                                    % Estimate_Process_Noise
scen.sig_v_rand = [0; 0];

%% Target Function definitions
% Define target turn sequence
function w = turn_seq(t)
    if t < 150
        w = 0;
    elseif t <= 350
        w = deg2rad(2*((t - 150) / 200));
    elseif t <= 550
        w = deg2rad(3*(((350 - t) / 200) + 1));
    elseif t < 700
        w = 0;
    elseif t <= 775
        w = -deg2rad(2);
    else
        w = 0;
    end
end

function u = accel_seq(t)
    u = 0;
end

% Convert turn sequence to normal acceleration
function u = accel_profile(t, y)
    a = accel_seq(t);
    speed = norm([y(2) y(4)]);
    w = turn_seq(t);
    u = [a; speed*w];
end


