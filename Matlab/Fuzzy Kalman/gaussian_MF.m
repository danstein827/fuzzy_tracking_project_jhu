function u = gaussian_MF(X, c, s)
        u = exp(-0.5*((X - c)/s).^2);
end