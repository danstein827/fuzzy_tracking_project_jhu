% Gets the error from a fuzzy kalman simulation
FKM.p_error = (sum((meas_hist(:, [4 6]) - meas_hist(:, [12 14])).^2, 2));
FKM.v_error = (sum((meas_hist(:, [5 7]) - meas_hist(:, [13 15])).^2, 2));
FKM.m_error = (sum((meas_hist(:, [4 6]) - meas_hist(:, [2 3])).^2, 2));
FKM.J1 = sqrt(sum(FKM.m_error/(length(FKM.m_error) - 1)));
FKM.J2 = [sqrt(sum(FKM.p_error/(length(FKM.p_error) - 1)));
    sqrt(sum(FKM.v_error/(length(FKM.v_error) - 1)))];