#ifndef RTW_HEADER_Target_Sim_2019a_h_
#define RTW_HEADER_Target_Sim_2019a_h_
#include <stddef.h>
#include <emmintrin.h>
#include <string.h>
#include "rtw_modelmap.h"
#ifndef Target_Sim_2019a_COMMON_INCLUDES_
#define Target_Sim_2019a_COMMON_INCLUDES_
#include <stdlib.h>
#include "rtwtypes.h"
#include "simtarget/slSimTgtSigstreamRTW.h"
#include "simtarget/slSimTgtSlioCoreRTW.h"
#include "simtarget/slSimTgtSlioClientsRTW.h"
#include "simtarget/slSimTgtSlioSdiRTW.h"
#include "sigstream_rtw.h"
#include "simstruc.h"
#include "fixedpoint.h"
#include "raccel.h"
#include "slsv_diagnostic_codegen_c_api.h"
#include "rt_logging.h"
#include "dt_info.h"
#include "ext_work.h"
#endif
#include "Target_Sim_2019a_types.h"
#include "multiword_types.h"
#include "mwmathutil.h"
#include "rt_defines.h"
#include "rtGetInf.h"
#include "rt_nonfinite.h"
#define MODEL_NAME Target_Sim_2019a
#define NSAMPLE_TIMES (3) 
#define NINPUTS (0)       
#define NOUTPUTS (0)     
#define NBLOCKIO (8) 
#define NUM_ZC_EVENTS (0) 
#ifndef NCSTATES
#define NCSTATES (5)   
#elif NCSTATES != 5
#error Invalid specification of NCSTATES defined in compiler command
#endif
#ifndef rtmGetDataMapInfo
#define rtmGetDataMapInfo(rtm) (*rt_dataMapInfoPtr)
#endif
#ifndef rtmSetDataMapInfo
#define rtmSetDataMapInfo(rtm, val) (rt_dataMapInfoPtr = &val)
#endif
#ifndef IN_RACCEL_MAIN
#endif
typedef struct { real_T dfhcidlurg ; real_T chczktztct [ 4 ] ; real_T
phys3gbgzb ; real_T aan0sy3tpg [ 2 ] ; real_T kuh3peukjd [ 2 ] ; real_T
f0mxoly50t [ 4 ] ; real_T dtkq3bia35 [ 4 ] ; real_T omrqpvzw4w [ 4 ] ; } B ;
typedef struct { real_T nufytm3oc5 ; struct { void * LoggedData ; }
chroivryh1 ; struct { void * LoggedData ; } jhv2ep41hb ; struct { void *
LoggedData ; } pwhfgbkzvd ; uint32_T g1kw2wymzr ; int_T aj4ztmxxjd ; } DW ;
typedef struct { real_T ohcfgpsi35 ; real_T ionnz4u3pk [ 4 ] ; } X ; typedef
struct { real_T ohcfgpsi35 ; real_T ionnz4u3pk [ 4 ] ; } XDot ; typedef
struct { boolean_T ohcfgpsi35 ; boolean_T ionnz4u3pk [ 4 ] ; } XDis ; typedef
struct { rtwCAPI_ModelMappingInfo mmi ; } DataMapInfo ; struct P_ { real_T
BandLimitedWhiteNoise_seed ; real_T SimTime_IC ; real_T
AccelerationLookup_tableData [ 801 ] ; real_T AccelerationLookup_bp01Data [
801 ] ; real_T TurnRateLookup_tableData [ 801 ] ; real_T
TurnRateLookup_bp01Data [ 801 ] ; real_T WhiteNoise_Mean ; real_T
WhiteNoise_StdDev ; real_T Output_Gain [ 2 ] ; real_T u_Value ; real_T
Constant_Value ; real_T StateTransition_Value [ 16 ] ; real_T Y0_Value [ 4 ]
; } ; extern const char * RT_MEMORY_ALLOCATION_ERROR ; extern B rtB ; extern
X rtX ; extern DW rtDW ; extern P rtP ; extern const
rtwCAPI_ModelMappingStaticInfo * Target_Sim_2019a_GetCAPIStaticMap ( void ) ;
extern SimStruct * const rtS ; extern const int_T gblNumToFiles ; extern
const int_T gblNumFrFiles ; extern const int_T gblNumFrWksBlocks ; extern
rtInportTUtable * gblInportTUtables ; extern const char * gblInportFileName ;
extern const int_T gblNumRootInportBlks ; extern const int_T
gblNumModelInputs ; extern const int_T gblInportDataTypeIdx [ ] ; extern
const int_T gblInportDims [ ] ; extern const int_T gblInportComplex [ ] ;
extern const int_T gblInportInterpoFlag [ ] ; extern const int_T
gblInportContinuous [ ] ; extern const int_T gblParameterTuningTid ; extern
DataMapInfo * rt_dataMapInfoPtr ; extern rtwCAPI_ModelMappingInfo *
rt_modelMapInfoPtr ; void MdlOutputs ( int_T tid ) ; void
MdlOutputsParameterSampleTime ( int_T tid ) ; void MdlUpdate ( int_T tid ) ;
void MdlTerminate ( void ) ; void MdlInitializeSizes ( void ) ; void
MdlInitializeSampleTimes ( void ) ; SimStruct * raccel_register_model (
ssExecutionInfo * executionInfo ) ;
#endif
