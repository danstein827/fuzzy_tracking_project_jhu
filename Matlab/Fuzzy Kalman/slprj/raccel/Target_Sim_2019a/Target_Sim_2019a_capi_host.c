#include "Target_Sim_2019a_capi_host.h"
static Target_Sim_2019a_host_DataMapInfo_T root;
static int initialized = 0;
rtwCAPI_ModelMappingInfo *getRootMappingInfo()
{
    if (initialized == 0) {
        initialized = 1;
        Target_Sim_2019a_host_InitializeDataMapInfo(&(root), "Target_Sim_2019a");
    }
    return &root.mmi;
}

rtwCAPI_ModelMappingInfo *mexFunction() {return(getRootMappingInfo());}
