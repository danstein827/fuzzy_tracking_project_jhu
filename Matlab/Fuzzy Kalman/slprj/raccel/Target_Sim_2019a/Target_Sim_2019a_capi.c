#include "rtw_capi.h"
#ifdef HOST_CAPI_BUILD
#include "Target_Sim_2019a_capi_host.h"
#define sizeof(s) ((size_t)(0xFFFF))
#undef rt_offsetof
#define rt_offsetof(s,el) ((uint16_T)(0xFFFF))
#define TARGET_CONST
#define TARGET_STRING(s) (s)    
#else
#include "builtin_typeid_types.h"
#include "Target_Sim_2019a.h"
#include "Target_Sim_2019a_capi.h"
#include "Target_Sim_2019a_private.h"
#ifdef LIGHT_WEIGHT_CAPI
#define TARGET_CONST                  
#define TARGET_STRING(s)               (NULL)                    
#else
#define TARGET_CONST                   const
#define TARGET_STRING(s)               (s)
#endif
#endif
static const rtwCAPI_Signals rtBlockSignals [ ] = { { 0 , 0 , TARGET_STRING (
"Target_Sim_2019a/Y0" ) , TARGET_STRING ( "" ) , 0 , 0 , 0 , 0 , 0 } , { 1 ,
0 , TARGET_STRING ( "Target_Sim_2019a/Target State" ) , TARGET_STRING ( "" )
, 0 , 0 , 0 , 0 , 1 } , { 2 , 0 , TARGET_STRING (
"Target_Sim_2019a/Acceleration Lookup" ) , TARGET_STRING ( "" ) , 0 , 0 , 1 ,
0 , 1 } , { 3 , 0 , TARGET_STRING ( "Target_Sim_2019a/Perpendicular Accel" )
, TARGET_STRING ( "" ) , 0 , 0 , 1 , 0 , 1 } , { 4 , 0 , TARGET_STRING (
"Target_Sim_2019a/Add" ) , TARGET_STRING ( "" ) , 0 , 0 , 0 , 0 , 1 } , { 5 ,
0 , TARGET_STRING ( "Target_Sim_2019a/Sum" ) , TARGET_STRING ( "" ) , 0 , 0 ,
0 , 0 , 1 } , { 6 , 0 , TARGET_STRING (
"Target_Sim_2019a/Band-Limited White Noise/Output" ) , TARGET_STRING ( "" ) ,
0 , 0 , 2 , 0 , 2 } , { 0 , 0 , ( NULL ) , ( NULL ) , 0 , 0 , 0 , 0 , 0 } } ;
static const rtwCAPI_BlockParameters rtBlockParameters [ ] = { { 7 ,
TARGET_STRING ( "Target_Sim_2019a/Band-Limited White Noise" ) , TARGET_STRING
( "seed" ) , 0 , 1 , 0 } , { 8 , TARGET_STRING ( "Target_Sim_2019a/1" ) ,
TARGET_STRING ( "Value" ) , 0 , 1 , 0 } , { 9 , TARGET_STRING (
"Target_Sim_2019a/Constant" ) , TARGET_STRING ( "Value" ) , 0 , 1 , 0 } , {
10 , TARGET_STRING ( "Target_Sim_2019a/State Transition" ) , TARGET_STRING (
"Value" ) , 0 , 3 , 0 } , { 11 , TARGET_STRING ( "Target_Sim_2019a/Y0" ) ,
TARGET_STRING ( "Value" ) , 0 , 0 , 0 } , { 12 , TARGET_STRING (
"Target_Sim_2019a/Sim Time" ) , TARGET_STRING ( "InitialCondition" ) , 0 , 1
, 0 } , { 13 , TARGET_STRING ( "Target_Sim_2019a/Acceleration Lookup" ) ,
TARGET_STRING ( "Table" ) , 0 , 4 , 0 } , { 14 , TARGET_STRING (
"Target_Sim_2019a/Acceleration Lookup" ) , TARGET_STRING (
"BreakpointsForDimension1" ) , 0 , 4 , 0 } , { 15 , TARGET_STRING (
"Target_Sim_2019a/Turn Rate  Lookup" ) , TARGET_STRING ( "Table" ) , 0 , 4 ,
0 } , { 16 , TARGET_STRING ( "Target_Sim_2019a/Turn Rate  Lookup" ) ,
TARGET_STRING ( "BreakpointsForDimension1" ) , 0 , 4 , 0 } , { 17 ,
TARGET_STRING ( "Target_Sim_2019a/Band-Limited White Noise/Output" ) ,
TARGET_STRING ( "Gain" ) , 0 , 2 , 0 } , { 18 , TARGET_STRING (
"Target_Sim_2019a/Band-Limited White Noise/White Noise" ) , TARGET_STRING (
"Mean" ) , 0 , 1 , 0 } , { 19 , TARGET_STRING (
"Target_Sim_2019a/Band-Limited White Noise/White Noise" ) , TARGET_STRING (
"StdDev" ) , 0 , 1 , 0 } , { 0 , ( NULL ) , ( NULL ) , 0 , 0 , 0 } } ; static
const rtwCAPI_Signals rtRootInputs [ ] = { { 0 , 0 , ( NULL ) , ( NULL ) , 0
, 0 , 0 , 0 , 0 } } ; static const rtwCAPI_Signals rtRootOutputs [ ] = { { 0
, 0 , ( NULL ) , ( NULL ) , 0 , 0 , 0 , 0 , 0 } } ; static const
rtwCAPI_ModelParameters rtModelParameters [ ] = { { 0 , ( NULL ) , 0 , 0 , 0
} } ;
#ifndef HOST_CAPI_BUILD
static void * rtDataAddrMap [ ] = { & rtB . omrqpvzw4w [ 0 ] , & rtB .
chczktztct [ 0 ] , & rtB . dfhcidlurg , & rtB . phys3gbgzb , & rtB .
dtkq3bia35 [ 0 ] , & rtB . f0mxoly50t [ 0 ] , & rtB . kuh3peukjd [ 0 ] , &
rtP . BandLimitedWhiteNoise_seed , & rtP . u_Value , & rtP . Constant_Value ,
& rtP . StateTransition_Value [ 0 ] , & rtP . Y0_Value [ 0 ] , & rtP .
SimTime_IC , & rtP . AccelerationLookup_tableData [ 0 ] , & rtP .
AccelerationLookup_bp01Data [ 0 ] , & rtP . TurnRateLookup_tableData [ 0 ] ,
& rtP . TurnRateLookup_bp01Data [ 0 ] , & rtP . Output_Gain [ 0 ] , & rtP .
WhiteNoise_Mean , & rtP . WhiteNoise_StdDev , } ; static int32_T *
rtVarDimsAddrMap [ ] = { ( NULL ) } ;
#endif
static TARGET_CONST rtwCAPI_DataTypeMap rtDataTypeMap [ ] = { { "double" ,
"real_T" , 0 , 0 , sizeof ( real_T ) , SS_DOUBLE , 0 , 0 , 0 } } ;
#ifdef HOST_CAPI_BUILD
#undef sizeof
#endif
static TARGET_CONST rtwCAPI_ElementMap rtElementMap [ ] = { { ( NULL ) , 0 ,
0 , 0 , 0 } , } ; static const rtwCAPI_DimensionMap rtDimensionMap [ ] = { {
rtwCAPI_VECTOR , 0 , 2 , 0 } , { rtwCAPI_SCALAR , 2 , 2 , 0 } , {
rtwCAPI_VECTOR , 4 , 2 , 0 } , { rtwCAPI_MATRIX_COL_MAJOR , 6 , 2 , 0 } , {
rtwCAPI_VECTOR , 8 , 2 , 0 } } ; static const uint_T rtDimensionArray [ ] = {
4 , 1 , 1 , 1 , 2 , 1 , 4 , 4 , 1 , 801 } ; static const real_T
rtcapiStoredFloats [ ] = { 0.0 , 1.0 } ; static const rtwCAPI_FixPtMap
rtFixPtMap [ ] = { { ( NULL ) , ( NULL ) , rtwCAPI_FIX_RESERVED , 0 , 0 , 0 }
, } ; static const rtwCAPI_SampleTimeMap rtSampleTimeMap [ ] = { { ( NULL ) ,
( NULL ) , 2 , 0 } , { ( const void * ) & rtcapiStoredFloats [ 0 ] , ( const
void * ) & rtcapiStoredFloats [ 0 ] , 0 , 0 } , { ( const void * ) &
rtcapiStoredFloats [ 1 ] , ( const void * ) & rtcapiStoredFloats [ 0 ] , 1 ,
0 } } ; static rtwCAPI_ModelMappingStaticInfo mmiStatic = { { rtBlockSignals
, 7 , rtRootInputs , 0 , rtRootOutputs , 0 } , { rtBlockParameters , 13 ,
rtModelParameters , 0 } , { ( NULL ) , 0 } , { rtDataTypeMap , rtDimensionMap
, rtFixPtMap , rtElementMap , rtSampleTimeMap , rtDimensionArray } , "float"
, { 342023695U , 1764360558U , 182658532U , 2998938698U } , ( NULL ) , 0 , 0
} ; const rtwCAPI_ModelMappingStaticInfo * Target_Sim_2019a_GetCAPIStaticMap
( void ) { return & mmiStatic ; }
#ifndef HOST_CAPI_BUILD
void Target_Sim_2019a_InitializeDataMapInfo ( void ) { rtwCAPI_SetVersion ( (
* rt_dataMapInfoPtr ) . mmi , 1 ) ; rtwCAPI_SetStaticMap ( ( *
rt_dataMapInfoPtr ) . mmi , & mmiStatic ) ; rtwCAPI_SetLoggingStaticMap ( ( *
rt_dataMapInfoPtr ) . mmi , ( NULL ) ) ; rtwCAPI_SetDataAddressMap ( ( *
rt_dataMapInfoPtr ) . mmi , rtDataAddrMap ) ; rtwCAPI_SetVarDimsAddressMap (
( * rt_dataMapInfoPtr ) . mmi , rtVarDimsAddrMap ) ;
rtwCAPI_SetInstanceLoggingInfo ( ( * rt_dataMapInfoPtr ) . mmi , ( NULL ) ) ;
rtwCAPI_SetChildMMIArray ( ( * rt_dataMapInfoPtr ) . mmi , ( NULL ) ) ;
rtwCAPI_SetChildMMIArrayLen ( ( * rt_dataMapInfoPtr ) . mmi , 0 ) ; }
#else
#ifdef __cplusplus
extern "C" {
#endif
void Target_Sim_2019a_host_InitializeDataMapInfo (
Target_Sim_2019a_host_DataMapInfo_T * dataMap , const char * path ) {
rtwCAPI_SetVersion ( dataMap -> mmi , 1 ) ; rtwCAPI_SetStaticMap ( dataMap ->
mmi , & mmiStatic ) ; rtwCAPI_SetDataAddressMap ( dataMap -> mmi , NULL ) ;
rtwCAPI_SetVarDimsAddressMap ( dataMap -> mmi , NULL ) ; rtwCAPI_SetPath (
dataMap -> mmi , path ) ; rtwCAPI_SetFullPath ( dataMap -> mmi , NULL ) ;
rtwCAPI_SetChildMMIArray ( dataMap -> mmi , ( NULL ) ) ;
rtwCAPI_SetChildMMIArrayLen ( dataMap -> mmi , 0 ) ; }
#ifdef __cplusplus
}
#endif
#endif
