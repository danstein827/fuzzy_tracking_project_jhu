#include "rt_logging_mmi.h"
#include "Target_Sim_2019a_capi.h"
#include <math.h>
#include "Target_Sim_2019a.h"
#include "Target_Sim_2019a_private.h"
#include "Target_Sim_2019a_dt.h"
#include "simstruc.h"
#include "fixedpoint.h"
#include "slsa_sim_engine.h"
extern void * CreateDiagnosticAsVoidPtr_wrapper ( const char * id , int nargs
, ... ) ; RTWExtModeInfo * gblRTWExtModeInfo = NULL ; void
raccelForceExtModeShutdown ( boolean_T extModeStartPktReceived ) { if ( !
extModeStartPktReceived ) { boolean_T stopRequested = false ;
rtExtModeWaitForStartPkt ( gblRTWExtModeInfo , 2 , & stopRequested ) ; }
rtExtModeShutdown ( 2 ) ; }
#include "slsv_diagnostic_codegen_c_api.h"
const int_T gblNumToFiles = 0 ; const int_T gblNumFrFiles = 0 ; const int_T
gblNumFrWksBlocks = 0 ;
#ifdef RSIM_WITH_SOLVER_MULTITASKING
boolean_T gbl_raccel_isMultitasking = 1 ;
#else
boolean_T gbl_raccel_isMultitasking = 0 ;
#endif
boolean_T gbl_raccel_tid01eq = 1 ; int_T gbl_raccel_NumST = 3 ; const char_T
* gbl_raccel_Version = "10.2 (R2020b) 29-Jul-2020" ; void
raccel_setup_MMIStateLog ( SimStruct * S ) {
#ifdef UseMMIDataLogging
rt_FillStateSigInfoFromMMI ( ssGetRTWLogInfo ( S ) , & ssGetErrorStatus ( S )
) ;
#else
UNUSED_PARAMETER ( S ) ;
#endif
} static DataMapInfo rt_dataMapInfo ; DataMapInfo * rt_dataMapInfoPtr = &
rt_dataMapInfo ; rtwCAPI_ModelMappingInfo * rt_modelMapInfoPtr = & (
rt_dataMapInfo . mmi ) ; const int_T gblNumRootInportBlks = 0 ; const int_T
gblNumModelInputs = 0 ; extern const char * gblInportFileName ; extern
rtInportTUtable * gblInportTUtables ; const int_T gblInportDataTypeIdx [ ] =
{ - 1 } ; const int_T gblInportDims [ ] = { - 1 } ; const int_T
gblInportComplex [ ] = { - 1 } ; const int_T gblInportInterpoFlag [ ] = { - 1
} ; const int_T gblInportContinuous [ ] = { - 1 } ; int_T enableFcnCallFlag [
] = { 1 , 1 , 1 } ; const char * raccelLoadInputsAndAperiodicHitTimes (
SimStruct * S , const char * inportFileName , int * matFileFormat ) { return
rt_RAccelReadInportsMatFile ( S , inportFileName , matFileFormat ) ; } B rtB
; X rtX ; DW rtDW ; static SimStruct model_S ; SimStruct * const rtS = &
model_S ; uint32_T plook_u32d_bincka ( real_T u , const real_T bp [ ] ,
uint32_T maxIndex ) { uint32_T bpIndex ; if ( u <= bp [ 0U ] ) { bpIndex = 0U
; } else if ( u < bp [ maxIndex ] ) { bpIndex = binsearch_u32d ( u , bp ,
maxIndex >> 1U , maxIndex ) ; } else { bpIndex = maxIndex ; } return bpIndex
; } uint32_T binsearch_u32d ( real_T u , const real_T bp [ ] , uint32_T
startIndex , uint32_T maxIndex ) { uint32_T bpIdx ; uint32_T bpIndex ;
uint32_T iRght ; bpIdx = startIndex ; bpIndex = 0U ; iRght = maxIndex ; while
( iRght - bpIndex > 1U ) { if ( u < bp [ bpIdx ] ) { iRght = bpIdx ; } else {
bpIndex = bpIdx ; } bpIdx = ( iRght + bpIndex ) >> 1U ; } return bpIndex ; }
real_T rt_urand_Upu32_Yd_f_pw_snf ( uint32_T * u ) { uint32_T hi ; uint32_T
lo ; lo = * u % 127773U * 16807U ; hi = * u / 127773U * 2836U ; if ( lo < hi
) { * u = 2147483647U - ( hi - lo ) ; } else { * u = lo - hi ; } return (
real_T ) * u * 4.6566128752457969E-10 ; } real_T rt_nrand_Upu32_Yd_f_pw_snf (
uint32_T * u ) { real_T si ; real_T sr ; real_T y ; do { sr = 2.0 *
rt_urand_Upu32_Yd_f_pw_snf ( u ) - 1.0 ; si = 2.0 *
rt_urand_Upu32_Yd_f_pw_snf ( u ) - 1.0 ; si = sr * sr + si * si ; } while (
si > 1.0 ) ; y = muDoubleScalarSqrt ( - 2.0 * muDoubleScalarLog ( si ) / si )
* sr ; return y ; } void MdlInitialize ( void ) { real_T tmp_p ; int32_T r ;
int32_T t ; uint32_T tseed ; boolean_T tmp ; rtX . ohcfgpsi35 = rtP .
SimTime_IC ; rtDW . aj4ztmxxjd = 1 ; if ( ssIsFirstInitCond ( rtS ) ) { rtX .
ionnz4u3pk [ 0 ] = 120.0 ; rtX . ionnz4u3pk [ 1 ] = - 0.3867 ; rtX .
ionnz4u3pk [ 2 ] = 60.0 ; rtX . ionnz4u3pk [ 3 ] = 0.0 ; tmp =
slIsRapidAcceleratorSimulating ( ) ; if ( tmp ) { tmp =
ssGetGlobalInitialStatesAvailable ( rtS ) ; rtDW . aj4ztmxxjd = ! tmp ; }
else { rtDW . aj4ztmxxjd = 1 ; } } tmp_p = muDoubleScalarFloor ( rtP .
BandLimitedWhiteNoise_seed ) ; if ( muDoubleScalarIsNaN ( tmp_p ) ||
muDoubleScalarIsInf ( tmp_p ) ) { tmp_p = 0.0 ; } else { tmp_p =
muDoubleScalarRem ( tmp_p , 4.294967296E+9 ) ; } tseed = tmp_p < 0.0 ? (
uint32_T ) - ( int32_T ) ( uint32_T ) - tmp_p : ( uint32_T ) tmp_p ; r = (
int32_T ) ( tseed >> 16U ) ; t = ( int32_T ) ( tseed & 32768U ) ; tseed = ( (
( ( tseed - ( ( uint32_T ) r << 16U ) ) + t ) << 16U ) + t ) + r ; if ( tseed
< 1U ) { tseed = 1144108930U ; } else { if ( tseed > 2147483646U ) { tseed =
2147483646U ; } } rtDW . g1kw2wymzr = tseed ; rtDW . nufytm3oc5 =
rt_nrand_Upu32_Yd_f_pw_snf ( & rtDW . g1kw2wymzr ) * rtP . WhiteNoise_StdDev
+ rtP . WhiteNoise_Mean ; } void MdlStart ( void ) { { bool
externalInputIsInDatasetFormat = false ; void * pISigstreamManager =
rt_GetISigstreamManager ( rtS ) ;
rtwISigstreamManagerGetInputIsInDatasetFormat ( pISigstreamManager , &
externalInputIsInDatasetFormat ) ; if ( externalInputIsInDatasetFormat ) { }
} { int_T dimensions [ 1 ] = { 2 } ; rtDW . chroivryh1 . LoggedData =
rt_CreateLogVar ( ssGetRTWLogInfo ( rtS ) , ssGetTStart ( rtS ) , ssGetTFinal
( rtS ) , 0.0 , ( & ssGetErrorStatus ( rtS ) ) , "Accel" , SS_DOUBLE , 0 , 0
, 0 , 2 , 1 , dimensions , NO_LOGVALDIMS , ( NULL ) , ( NULL ) , 0 , 1 , 1.0
, 1 ) ; if ( rtDW . chroivryh1 . LoggedData == ( NULL ) ) return ;
ssCachePointer ( rtS , & ( rtDW . chroivryh1 . LoggedData ) ) ; } { int_T
dimensions [ 1 ] = { 4 } ; rtDW . jhv2ep41hb . LoggedData = rt_CreateLogVar (
ssGetRTWLogInfo ( rtS ) , ssGetTStart ( rtS ) , ssGetTFinal ( rtS ) , 0.0 , (
& ssGetErrorStatus ( rtS ) ) , "Y" , SS_DOUBLE , 0 , 0 , 0 , 4 , 1 ,
dimensions , NO_LOGVALDIMS , ( NULL ) , ( NULL ) , 0 , 1 , 1.0 , 1 ) ; if (
rtDW . jhv2ep41hb . LoggedData == ( NULL ) ) return ; ssCachePointer ( rtS ,
& ( rtDW . jhv2ep41hb . LoggedData ) ) ; } { int_T dimensions [ 1 ] = { 4 } ;
rtDW . pwhfgbkzvd . LoggedData = rt_CreateLogVar ( ssGetRTWLogInfo ( rtS ) ,
ssGetTStart ( rtS ) , ssGetTFinal ( rtS ) , 0.0 , ( & ssGetErrorStatus ( rtS
) ) , "Accl_Cart" , SS_DOUBLE , 0 , 0 , 0 , 4 , 1 , dimensions ,
NO_LOGVALDIMS , ( NULL ) , ( NULL ) , 0 , 1 , 1.0 , 1 ) ; if ( rtDW .
pwhfgbkzvd . LoggedData == ( NULL ) ) return ; ssCachePointer ( rtS , & (
rtDW . pwhfgbkzvd . LoggedData ) ) ; } rtB . omrqpvzw4w [ 0 ] = rtP .
Y0_Value [ 0 ] ; rtB . omrqpvzw4w [ 1 ] = rtP . Y0_Value [ 1 ] ; rtB .
omrqpvzw4w [ 2 ] = rtP . Y0_Value [ 2 ] ; rtB . omrqpvzw4w [ 3 ] = rtP .
Y0_Value [ 3 ] ; MdlInitialize ( ) ; } void MdlOutputs ( int_T tid ) {
__m128d tmp_e ; __m128d tmp_p ; real_T tmp [ 8 ] ; real_T fo042y2zpj [ 4 ] ;
real_T b2izcbhwl3 ; real_T doigvl4hz0_idx_0 ; int32_T i ; rtB . dfhcidlurg =
rtP . AccelerationLookup_tableData [ plook_u32d_bincka ( rtX . ohcfgpsi35 ,
rtP . AccelerationLookup_bp01Data , 800U ) ] ; if ( ssIsMajorTimeStep ( rtS )
) { if ( rtDW . aj4ztmxxjd != 0 ) { rtX . ionnz4u3pk [ 0 ] = rtB . omrqpvzw4w
[ 0 ] ; rtX . ionnz4u3pk [ 1 ] = rtB . omrqpvzw4w [ 1 ] ; rtX . ionnz4u3pk [
2 ] = rtB . omrqpvzw4w [ 2 ] ; rtX . ionnz4u3pk [ 3 ] = rtB . omrqpvzw4w [ 3
] ; } rtB . chczktztct [ 0 ] = rtX . ionnz4u3pk [ 0 ] ; rtB . chczktztct [ 1
] = rtX . ionnz4u3pk [ 1 ] ; rtB . chczktztct [ 2 ] = rtX . ionnz4u3pk [ 2 ]
; rtB . chczktztct [ 3 ] = rtX . ionnz4u3pk [ 3 ] ; } else { rtB . chczktztct
[ 0 ] = rtX . ionnz4u3pk [ 0 ] ; rtB . chczktztct [ 1 ] = rtX . ionnz4u3pk [
1 ] ; rtB . chczktztct [ 2 ] = rtX . ionnz4u3pk [ 2 ] ; rtB . chczktztct [ 3
] = rtX . ionnz4u3pk [ 3 ] ; } b2izcbhwl3 = muDoubleScalarSqrt ( rtB .
chczktztct [ 1 ] * rtB . chczktztct [ 1 ] + rtB . chczktztct [ 3 ] * rtB .
chczktztct [ 3 ] ) ; rtB . phys3gbgzb = b2izcbhwl3 * rtP .
TurnRateLookup_tableData [ plook_u32d_bincka ( rtX . ohcfgpsi35 , rtP .
TurnRateLookup_bp01Data , 800U ) ] ; if ( ssIsSampleHit ( rtS , 1 , 0 ) ) {
rtB . aan0sy3tpg [ 0 ] = rtB . dfhcidlurg ; rtB . aan0sy3tpg [ 1 ] = rtB .
phys3gbgzb ; if ( ssGetLogOutput ( rtS ) ) { { double locTime = ssGetTaskTime
( rtS , 1 ) ; ; if ( rtwTimeInLoggingInterval ( rtliGetLoggingInterval (
ssGetRootSS ( rtS ) -> mdlInfo -> rtwLogInfo ) , locTime ) ) {
rt_UpdateLogVar ( ( LogVar * ) ( LogVar * ) ( rtDW . chroivryh1 . LoggedData
) , & rtB . aan0sy3tpg [ 0 ] , 0 ) ; } } } if ( ssGetLogOutput ( rtS ) ) { {
double locTime = ssGetTaskTime ( rtS , 1 ) ; ; if ( rtwTimeInLoggingInterval
( rtliGetLoggingInterval ( ssGetRootSS ( rtS ) -> mdlInfo -> rtwLogInfo ) ,
locTime ) ) { rt_UpdateLogVar ( ( LogVar * ) ( LogVar * ) ( rtDW . jhv2ep41hb
. LoggedData ) , & rtB . chczktztct [ 0 ] , 0 ) ; } } } rtB . kuh3peukjd [ 0
] = rtP . Output_Gain [ 0 ] * rtDW . nufytm3oc5 ; rtB . kuh3peukjd [ 1 ] =
rtP . Output_Gain [ 1 ] * rtDW . nufytm3oc5 ; } doigvl4hz0_idx_0 = rtB .
chczktztct [ 1 ] / b2izcbhwl3 ; b2izcbhwl3 = rtB . chczktztct [ 3 ] /
b2izcbhwl3 ; tmp [ 1 ] = doigvl4hz0_idx_0 ; tmp [ 5 ] = - b2izcbhwl3 ; tmp [
0 ] = 0.0 ; tmp [ 2 ] = 0.0 ; tmp [ 4 ] = 0.0 ; tmp [ 6 ] = 0.0 ; tmp [ 3 ] =
b2izcbhwl3 ; tmp [ 7 ] = doigvl4hz0_idx_0 ; for ( i = 0 ; i <= 2 ; i += 2 ) {
tmp_p = _mm_loadu_pd ( & tmp [ i ] ) ; tmp_e = _mm_loadu_pd ( & tmp [ i + 4 ]
) ; _mm_storeu_pd ( & fo042y2zpj [ i ] , _mm_add_pd ( _mm_mul_pd ( tmp_e ,
_mm_set1_pd ( rtB . phys3gbgzb ) ) , _mm_add_pd ( _mm_mul_pd ( tmp_p ,
_mm_set1_pd ( rtB . dfhcidlurg ) ) , _mm_set1_pd ( 0.0 ) ) ) ) ; } rtB .
f0mxoly50t [ 0 ] = rtP . Constant_Value + fo042y2zpj [ 0 ] ; rtB . f0mxoly50t
[ 1 ] = rtB . kuh3peukjd [ 0 ] + fo042y2zpj [ 1 ] ; rtB . f0mxoly50t [ 2 ] =
rtP . Constant_Value + fo042y2zpj [ 2 ] ; rtB . f0mxoly50t [ 3 ] = rtB .
kuh3peukjd [ 1 ] + fo042y2zpj [ 3 ] ; if ( ssIsSampleHit ( rtS , 1 , 0 ) ) {
if ( ssGetLogOutput ( rtS ) ) { { double locTime = ssGetTaskTime ( rtS , 1 )
; ; if ( rtwTimeInLoggingInterval ( rtliGetLoggingInterval ( ssGetRootSS (
rtS ) -> mdlInfo -> rtwLogInfo ) , locTime ) ) { rt_UpdateLogVar ( ( LogVar *
) ( LogVar * ) ( rtDW . pwhfgbkzvd . LoggedData ) , & rtB . f0mxoly50t [ 0 ]
, 0 ) ; } } } } for ( i = 0 ; i <= 2 ; i += 2 ) { tmp_p = _mm_loadu_pd ( &
rtB . f0mxoly50t [ i ] ) ; _mm_storeu_pd ( & rtB . dtkq3bia35 [ i ] ,
_mm_add_pd ( _mm_add_pd ( _mm_add_pd ( _mm_add_pd ( _mm_mul_pd ( _mm_loadu_pd
( & rtP . StateTransition_Value [ i + 4 ] ) , _mm_set1_pd ( rtB . chczktztct
[ 1 ] ) ) , _mm_mul_pd ( _mm_loadu_pd ( & rtP . StateTransition_Value [ i ] )
, _mm_set1_pd ( rtB . chczktztct [ 0 ] ) ) ) , _mm_mul_pd ( _mm_loadu_pd ( &
rtP . StateTransition_Value [ i + 8 ] ) , _mm_set1_pd ( rtB . chczktztct [ 2
] ) ) ) , _mm_mul_pd ( _mm_loadu_pd ( & rtP . StateTransition_Value [ i + 12
] ) , _mm_set1_pd ( rtB . chczktztct [ 3 ] ) ) ) , tmp_p ) ) ; }
UNUSED_PARAMETER ( tid ) ; } void MdlOutputsTID2 ( int_T tid ) { rtB .
omrqpvzw4w [ 0 ] = rtP . Y0_Value [ 0 ] ; rtB . omrqpvzw4w [ 1 ] = rtP .
Y0_Value [ 1 ] ; rtB . omrqpvzw4w [ 2 ] = rtP . Y0_Value [ 2 ] ; rtB .
omrqpvzw4w [ 3 ] = rtP . Y0_Value [ 3 ] ; UNUSED_PARAMETER ( tid ) ; } void
MdlUpdate ( int_T tid ) { rtDW . aj4ztmxxjd = 0 ; if ( ssIsSampleHit ( rtS ,
1 , 0 ) ) { rtDW . nufytm3oc5 = rt_nrand_Upu32_Yd_f_pw_snf ( & rtDW .
g1kw2wymzr ) * rtP . WhiteNoise_StdDev + rtP . WhiteNoise_Mean ; }
UNUSED_PARAMETER ( tid ) ; } void MdlUpdateTID2 ( int_T tid ) {
UNUSED_PARAMETER ( tid ) ; } void MdlDerivatives ( void ) { XDot * _rtXdot ;
_rtXdot = ( ( XDot * ) ssGetdX ( rtS ) ) ; _rtXdot -> ohcfgpsi35 = rtP .
u_Value ; _rtXdot -> ionnz4u3pk [ 0 ] = rtB . dtkq3bia35 [ 0 ] ; _rtXdot ->
ionnz4u3pk [ 1 ] = rtB . dtkq3bia35 [ 1 ] ; _rtXdot -> ionnz4u3pk [ 2 ] = rtB
. dtkq3bia35 [ 2 ] ; _rtXdot -> ionnz4u3pk [ 3 ] = rtB . dtkq3bia35 [ 3 ] ; }
void MdlProjection ( void ) { } void MdlTerminate ( void ) { } void
MdlInitializeSizes ( void ) { ssSetNumContStates ( rtS , 5 ) ;
ssSetNumPeriodicContStates ( rtS , 0 ) ; ssSetNumY ( rtS , 0 ) ; ssSetNumU (
rtS , 0 ) ; ssSetDirectFeedThrough ( rtS , 0 ) ; ssSetNumSampleTimes ( rtS ,
2 ) ; ssSetNumBlocks ( rtS , 26 ) ; ssSetNumBlockIO ( rtS , 8 ) ;
ssSetNumBlockParams ( rtS , 3232 ) ; } void MdlInitializeSampleTimes ( void )
{ ssSetSampleTime ( rtS , 0 , 0.0 ) ; ssSetSampleTime ( rtS , 1 , 1.0 ) ;
ssSetOffsetTime ( rtS , 0 , 0.0 ) ; ssSetOffsetTime ( rtS , 1 , 0.0 ) ; }
void raccel_set_checksum ( ) { ssSetChecksumVal ( rtS , 0 , 342023695U ) ;
ssSetChecksumVal ( rtS , 1 , 1764360558U ) ; ssSetChecksumVal ( rtS , 2 ,
182658532U ) ; ssSetChecksumVal ( rtS , 3 , 2998938698U ) ; }
#if defined(_MSC_VER)
#pragma optimize( "", off )
#endif
SimStruct * raccel_register_model ( ssExecutionInfo * executionInfo ) {
static struct _ssMdlInfo mdlInfo ; ( void ) memset ( ( char * ) rtS , 0 ,
sizeof ( SimStruct ) ) ; ( void ) memset ( ( char * ) & mdlInfo , 0 , sizeof
( struct _ssMdlInfo ) ) ; ssSetMdlInfoPtr ( rtS , & mdlInfo ) ;
ssSetExecutionInfo ( rtS , executionInfo ) ; { static time_T mdlPeriod [
NSAMPLE_TIMES ] ; static time_T mdlOffset [ NSAMPLE_TIMES ] ; static time_T
mdlTaskTimes [ NSAMPLE_TIMES ] ; static int_T mdlTsMap [ NSAMPLE_TIMES ] ;
static int_T mdlSampleHits [ NSAMPLE_TIMES ] ; static boolean_T
mdlTNextWasAdjustedPtr [ NSAMPLE_TIMES ] ; static int_T mdlPerTaskSampleHits
[ NSAMPLE_TIMES * NSAMPLE_TIMES ] ; static time_T mdlTimeOfNextSampleHit [
NSAMPLE_TIMES ] ; { int_T i ; for ( i = 0 ; i < NSAMPLE_TIMES ; i ++ ) {
mdlPeriod [ i ] = 0.0 ; mdlOffset [ i ] = 0.0 ; mdlTaskTimes [ i ] = 0.0 ;
mdlTsMap [ i ] = i ; mdlSampleHits [ i ] = 1 ; } } ssSetSampleTimePtr ( rtS ,
& mdlPeriod [ 0 ] ) ; ssSetOffsetTimePtr ( rtS , & mdlOffset [ 0 ] ) ;
ssSetSampleTimeTaskIDPtr ( rtS , & mdlTsMap [ 0 ] ) ; ssSetTPtr ( rtS , &
mdlTaskTimes [ 0 ] ) ; ssSetSampleHitPtr ( rtS , & mdlSampleHits [ 0 ] ) ;
ssSetTNextWasAdjustedPtr ( rtS , & mdlTNextWasAdjustedPtr [ 0 ] ) ;
ssSetPerTaskSampleHitsPtr ( rtS , & mdlPerTaskSampleHits [ 0 ] ) ;
ssSetTimeOfNextSampleHitPtr ( rtS , & mdlTimeOfNextSampleHit [ 0 ] ) ; }
ssSetSolverMode ( rtS , SOLVER_MODE_SINGLETASKING ) ; { ssSetBlockIO ( rtS ,
( ( void * ) & rtB ) ) ; ssSetWorkSizeInBytes ( rtS , sizeof ( rtB ) ,
"BlockIO" ) ; ( void ) memset ( ( ( void * ) & rtB ) , 0 , sizeof ( B ) ) ; }
{ real_T * x = ( real_T * ) & rtX ; ssSetContStates ( rtS , x ) ; ( void )
memset ( ( void * ) x , 0 , sizeof ( X ) ) ; } { void * dwork = ( void * ) &
rtDW ; ssSetRootDWork ( rtS , dwork ) ; ssSetWorkSizeInBytes ( rtS , sizeof (
rtDW ) , "DWork" ) ; ( void ) memset ( dwork , 0 , sizeof ( DW ) ) ; } {
static DataTypeTransInfo dtInfo ; ( void ) memset ( ( char_T * ) & dtInfo , 0
, sizeof ( dtInfo ) ) ; ssSetModelMappingInfo ( rtS , & dtInfo ) ; dtInfo .
numDataTypes = 14 ; dtInfo . dataTypeSizes = & rtDataTypeSizes [ 0 ] ; dtInfo
. dataTypeNames = & rtDataTypeNames [ 0 ] ; dtInfo . BTransTable = &
rtBTransTable ; dtInfo . PTransTable = & rtPTransTable ; dtInfo .
dataTypeInfoTable = rtDataTypeInfoTable ; }
Target_Sim_2019a_InitializeDataMapInfo ( ) ; ssSetIsRapidAcceleratorActive (
rtS , true ) ; ssSetRootSS ( rtS , rtS ) ; ssSetVersion ( rtS ,
SIMSTRUCT_VERSION_LEVEL2 ) ; ssSetModelName ( rtS , "Target_Sim_2019a" ) ;
ssSetPath ( rtS , "Target_Sim_2019a" ) ; ssSetTStart ( rtS , 0.0 ) ;
ssSetTFinal ( rtS , 800.0 ) ; ssSetStepSize ( rtS , 1.0 ) ;
ssSetFixedStepSize ( rtS , 1.0 ) ; { static RTWLogInfo rt_DataLoggingInfo ;
rt_DataLoggingInfo . loggingInterval = NULL ; ssSetRTWLogInfo ( rtS , &
rt_DataLoggingInfo ) ; } { { static int_T rt_LoggedStateWidths [ ] = { 1 , 4
} ; static int_T rt_LoggedStateNumDimensions [ ] = { 1 , 1 } ; static int_T
rt_LoggedStateDimensions [ ] = { 1 , 4 } ; static boolean_T
rt_LoggedStateIsVarDims [ ] = { 0 , 0 } ; static BuiltInDTypeId
rt_LoggedStateDataTypeIds [ ] = { SS_DOUBLE , SS_DOUBLE } ; static int_T
rt_LoggedStateComplexSignals [ ] = { 0 , 0 } ; static RTWPreprocessingFcnPtr
rt_LoggingStatePreprocessingFcnPtrs [ ] = { ( NULL ) , ( NULL ) } ; static
const char_T * rt_LoggedStateLabels [ ] = { "CSTATE" , "CSTATE" } ; static
const char_T * rt_LoggedStateBlockNames [ ] = { "Target_Sim_2019a/Sim Time" ,
"Target_Sim_2019a/Target State" } ; static const char_T * rt_LoggedStateNames
[ ] = { "" , "" } ; static boolean_T rt_LoggedStateCrossMdlRef [ ] = { 0 , 0
} ; static RTWLogDataTypeConvert rt_RTWLogDataTypeConvert [ ] = { { 0 ,
SS_DOUBLE , SS_DOUBLE , 0 , 0 , 0 , 1.0 , 0 , 0.0 } , { 0 , SS_DOUBLE ,
SS_DOUBLE , 0 , 0 , 0 , 1.0 , 0 , 0.0 } } ; static RTWLogSignalInfo
rt_LoggedStateSignalInfo = { 2 , rt_LoggedStateWidths ,
rt_LoggedStateNumDimensions , rt_LoggedStateDimensions ,
rt_LoggedStateIsVarDims , ( NULL ) , ( NULL ) , rt_LoggedStateDataTypeIds ,
rt_LoggedStateComplexSignals , ( NULL ) , rt_LoggingStatePreprocessingFcnPtrs
, { rt_LoggedStateLabels } , ( NULL ) , ( NULL ) , ( NULL ) , {
rt_LoggedStateBlockNames } , { rt_LoggedStateNames } ,
rt_LoggedStateCrossMdlRef , rt_RTWLogDataTypeConvert } ; static void *
rt_LoggedStateSignalPtrs [ 2 ] ; rtliSetLogXSignalPtrs ( ssGetRTWLogInfo (
rtS ) , ( LogSignalPtrsType ) rt_LoggedStateSignalPtrs ) ;
rtliSetLogXSignalInfo ( ssGetRTWLogInfo ( rtS ) , & rt_LoggedStateSignalInfo
) ; rt_LoggedStateSignalPtrs [ 0 ] = ( void * ) & rtX . ohcfgpsi35 ;
rt_LoggedStateSignalPtrs [ 1 ] = ( void * ) & rtX . ionnz4u3pk [ 0 ] ; }
rtliSetLogT ( ssGetRTWLogInfo ( rtS ) , "tout" ) ; rtliSetLogX (
ssGetRTWLogInfo ( rtS ) , "" ) ; rtliSetLogXFinal ( ssGetRTWLogInfo ( rtS ) ,
"" ) ; rtliSetLogVarNameModifier ( ssGetRTWLogInfo ( rtS ) , "none" ) ;
rtliSetLogFormat ( ssGetRTWLogInfo ( rtS ) , 4 ) ; rtliSetLogMaxRows (
ssGetRTWLogInfo ( rtS ) , 0 ) ; rtliSetLogDecimation ( ssGetRTWLogInfo ( rtS
) , 1 ) ; rtliSetLogY ( ssGetRTWLogInfo ( rtS ) , "" ) ;
rtliSetLogYSignalInfo ( ssGetRTWLogInfo ( rtS ) , ( NULL ) ) ;
rtliSetLogYSignalPtrs ( ssGetRTWLogInfo ( rtS ) , ( NULL ) ) ; } { static
struct _ssStatesInfo2 statesInfo2 ; ssSetStatesInfo2 ( rtS , & statesInfo2 )
; } { static ssPeriodicStatesInfo periodicStatesInfo ;
ssSetPeriodicStatesInfo ( rtS , & periodicStatesInfo ) ; } { static
ssJacobianPerturbationBounds jacobianPerturbationBounds ;
ssSetJacobianPerturbationBounds ( rtS , & jacobianPerturbationBounds ) ; } {
static ssSolverInfo slvrInfo ; static boolean_T contStatesDisabled [ 5 ] ;
ssSetSolverInfo ( rtS , & slvrInfo ) ; ssSetSolverName ( rtS , "ode3" ) ;
ssSetVariableStepSolver ( rtS , 0 ) ; ssSetSolverConsistencyChecking ( rtS ,
0 ) ; ssSetSolverAdaptiveZcDetection ( rtS , 0 ) ;
ssSetSolverRobustResetMethod ( rtS , 0 ) ; ssSetSolverStateProjection ( rtS ,
0 ) ; ssSetSolverMassMatrixType ( rtS , ( ssMatrixType ) 0 ) ;
ssSetSolverMassMatrixNzMax ( rtS , 0 ) ; ssSetModelOutputs ( rtS , MdlOutputs
) ; ssSetModelLogData ( rtS , rt_UpdateTXYLogVars ) ;
ssSetModelLogDataIfInInterval ( rtS , rt_UpdateTXXFYLogVars ) ;
ssSetModelUpdate ( rtS , MdlUpdate ) ; ssSetModelDerivatives ( rtS ,
MdlDerivatives ) ; ssSetTNextTid ( rtS , INT_MIN ) ; ssSetTNext ( rtS ,
rtMinusInf ) ; ssSetSolverNeedsReset ( rtS ) ; ssSetNumNonsampledZCs ( rtS ,
0 ) ; ssSetContStateDisabled ( rtS , contStatesDisabled ) ; }
ssSetChecksumVal ( rtS , 0 , 342023695U ) ; ssSetChecksumVal ( rtS , 1 ,
1764360558U ) ; ssSetChecksumVal ( rtS , 2 , 182658532U ) ; ssSetChecksumVal
( rtS , 3 , 2998938698U ) ; { static const sysRanDType rtAlwaysEnabled =
SUBSYS_RAN_BC_ENABLE ; static RTWExtModeInfo rt_ExtModeInfo ; static const
sysRanDType * systemRan [ 2 ] ; gblRTWExtModeInfo = & rt_ExtModeInfo ;
ssSetRTWExtModeInfo ( rtS , & rt_ExtModeInfo ) ;
rteiSetSubSystemActiveVectorAddresses ( & rt_ExtModeInfo , systemRan ) ;
systemRan [ 0 ] = & rtAlwaysEnabled ; systemRan [ 1 ] = & rtAlwaysEnabled ;
rteiSetModelMappingInfoPtr ( ssGetRTWExtModeInfo ( rtS ) , &
ssGetModelMappingInfo ( rtS ) ) ; rteiSetChecksumsPtr ( ssGetRTWExtModeInfo (
rtS ) , ssGetChecksums ( rtS ) ) ; rteiSetTPtr ( ssGetRTWExtModeInfo ( rtS )
, ssGetTPtr ( rtS ) ) ; } return rtS ; }
#if defined(_MSC_VER)
#pragma optimize( "", on )
#endif
const int_T gblParameterTuningTid = 2 ; void MdlOutputsParameterSampleTime (
int_T tid ) { MdlOutputsTID2 ( tid ) ; }
