% Check if seed has been created, for when not running via monte-carlo
if not(exist('seed'))
    seed = 1234632;
end
rng(seed);

% Check if a simulation file as been loaded
if not(exist('scen'))
    Scenario1
end

% Setup time variable
t = 0:scen.dt:scen.t_end;

% tic
% Run target simulation
if not(exist('y'))
    [t, y] = ode113(scen.target_dyn, t, scen.Y0);
end
% toc

% Initial estimate is blank
y_hat = [0; 0; 0; 0];

sig_est = ([scen.sig_v; scen.sig_v] + scen.sig_v_rand);

Q = scen.G*diag(sig_est.^2)*scen.G';
R = eye(2)*scen.sig_m^2;
P = 4*eye(4);

n_meas = 0;     % Number of radar measurements taken
next_t_measure = 0; % Time the next measurement should be taken

% Initialize history arrays
y_hat_hist = zeros(size(y));
k_hist = zeros([0 3]);
meas_hist = zeros([0 15]);
sig_hist = [];

Nr = 30;
r_buffer = zeros([Nr 2]);
laglen = Nr*2;
alpha = 0;
dsig_last = [0; 0];

% tic
% Run the time simulation
for i = 1:length(t)
    % Check if it is time to take a radar measurement
    if t(i) >= next_t_measure
        n_meas = n_meas + 1;    % Increment number of measurements
        next_t_measure = next_t_measure + scen.dt_m; % Schedule next measurement
        % Take a measurement
        measure = scen.H*y(i,:)' + scen.sig_m*randn([2 1]);
        
        % Measure History
        % C 1 - Time
        % C 2-3 - Radar measurement at t
        % C 4-7 - Target actual postion
        % C 8-11 - Target estimate pre-adjustment
        % C 12-15 - Target estimate post-adjustment
        meas_hist(end+1, 1:11) = [t(i) measure' y(i,:) y_hat'];
        
        r = measure - scen.H*y_hat;
        r_buffer(mod(n_meas-1, Nr)+1, :) = r';
        
        k = P*scen.H'/(R + scen.H*P*scen.H');
        y_hat = y_hat + k*r;
        meas_hist(end,12:15) = y_hat';
        P = (eye(4) - k*scen.H)*P;

        C = 0;
        for ir = 1:Nr
            C = C + (r_buffer(ir,:)'*r_buffer(ir,:))/Nr;
        end
        S = scen.H*(scen.Fm*P*scen.Fm' + Q)*scen.H' + R;
        
        DoM = diag(S - C);
        
        lag = exp(-((laglen)-min(n_meas, laglen))/10);
%         lag = min(((n_meas - laglen)/laglen)+1, 1);
        dsig = lag*[Q_FIE(DoM(1)); Q_FIE(DoM(2))];
        
%         if sign(dsig) == sign(dsig_last)
%             alpha = alpha + 0.001;
%         else
%             alpha = 0;
%         end
        
        dsig_last = dsig;
        
        sig_est = sig_est.*max((1 + dsig), 0.01);
        sig_hist(end+1,:) = sig_est';
        
        Q = scen.G*diag(sig_est.^2)*scen.G';
        
    end
    
    y_hat_hist(i,:) = y_hat';
    
    y_hat = scen.F*y_hat;
    P = scen.F*P*scen.F' + Q;
end
% toc