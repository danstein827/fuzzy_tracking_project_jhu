a1 = logspace(-1,2,20);
a2 = logspace(-1,2,21);



for ii = 1:20
    for jj = 1:20
        alpha = [a1(ii); a2(jj)];
        Fuzzy_Kalman_Monte_Carlo;
        
        m_e(ii,jj) = mean(meas_Error_RMS);
        e_e(ii,jj) = mean(pos_Error_RMS);        
    end
    disp("\n Completed " + ii);
end
