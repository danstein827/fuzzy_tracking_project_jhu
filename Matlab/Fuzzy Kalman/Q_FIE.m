function dq = Q_FIE(dom)
%     m = [ls_gaussian_MF(dom, -1, 0.5);
%         gaussian_MF(dom, -0.3, 0.05);
%         gaussian_MF(dom, -0.15, 0.05);
%         gaussian_MF(dom, 0, 0.05);
%         gaussian_MF(dom, 0.15, 0.05);
%         gaussian_MF(dom, 0.3, 0.05);
%         rs_gaussian_MF(dom, 1, 0.5)];
%     
%     % CA Defuzz
%     c = [0.5 .12 .02 0 -.02 -.12 -0.5];
%     
%     dq = c*m/sum(m);
    
    m = [ls_gaussian_MF(dom, -0.4, 0.05);
        gaussian_MF(dom, -0.2, 0.05);
        gaussian_MF(dom, -0.05, 0.05)
        gaussian_MF(dom, 0, 0.05);
        gaussian_MF(dom, 0.05, 0.05)
        gaussian_MF(dom, 0.2, 0.05);
        rs_gaussian_MF(dom, 0.4, 0.05)];
    
    % CA Defuzz
    c = [.5 .2 0.1 0 -0.1 -.2 -.5];
    
    dq = c*m/sum(m);



end