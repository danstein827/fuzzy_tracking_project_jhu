% Check if seed has been created, for when not running via monte-carlo
if not(exist('seed'))
    seed = 1234632;
end
rng(seed);

% Check if a simulation file as been loaded
if not(exist('scen'))
    Scenario1
end

% Setup time variable
t = 0:scen.dt:scen.t_end;

% Run target simulation
if not(exist('y'))
    [t, y] = ode113(scen.target_dyn, t, scen.Y0);
end

n_meas = 0;     % Number of radar measurements taken
next_t_measure = 0; % Time the next measurement should be taken
F = @(y) polarVTurn(y, scen.dt);
dF = @(y) polarVTurn_Jac(y, scen.dt);
Q = (scen.sig_v+sum(scen.sig_v_rand))^2 * ...
        blkdiag(zeros(2), ...
        scen.dt^2, ...
        [scen.dt^3/3 scen.dt^2/2;
        scen.dt^2/2 scen.dt^2]);
R = eye(2)*scen.sig_m^2;
P = 4*eye(5);
H = [1 0 0 0 0;
    0 1 0 0 0];

ekf = myExtendedKalmanFilter(F, dF, H, P, Q, R, zeros([5 1]), scen.dt);

% Initialize history arrays
y_hat_hist = zeros([length(t) 5]);
meas_hist = zeros([0 17]);

for i = 1:length(t)
    % Check if it is time to take a radar measurement
    if t(i) >= next_t_measure
        n_meas = n_meas + 1;    % Increment number of measurements
        next_t_measure = next_t_measure + scen.dt_m; % Schedule next measurement
        % Take a measurement
        measure = scen.H*y(i,:)' + scen.sig_m*randn([2 1]);
        
        % Measure History
        % C 1 - Time
        % C 2-3 - Radar measurement at t
        % C 4-7 - Target actual postion
        % C 8-11 - Target estimate pre-adjustment
        % C 12-15 - Target estimate post-adjustment
        meas_hist(end+1, 1:12) = [t(i) measure' y(i,:) ekf.y_hat'];        
        ekf.update(measure);
        meas_hist(end, 13:17) = ekf.y_hat';
    end        
    
    y_hat_hist(i,:) = ekf.y_hat';
    
    ekf.step();
    
end