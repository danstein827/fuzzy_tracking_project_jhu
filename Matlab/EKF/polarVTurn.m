function y_next = polarVTurn(y, T)
    eps = 0.000001;
    X = y(1);
    Y = y(2);
    V = y(3);
    th = y(4);
    w = y(5);
    
    if abs(w) > eps
        y_next = [X + (2*V/w)*sin(w*T/2)*cos(th + w*T/2);
                  Y + (2*V/w)*sin(w*T/2)*sin(th + w*T/2);
                  V;
                  th + w*T;
                  w];
    else
        y_next = [X + V*T*cos(th);
                  Y + V*T*sin(th);
                  V
                  th + w*T;
                  w];
    end    
end