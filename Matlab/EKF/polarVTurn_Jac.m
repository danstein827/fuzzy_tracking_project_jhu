function dF = polarVTurn_Jac(y, T)
    eps = 0.000001;
    X = y(1);
    Y = y(2);
    V = y(3);
    th = y(4);
    w = y(5);
    
    dF = zeros([5 5]);
    
    dF(:,1) = [1; 0; 0; 0; 0]; % df/dx
    dF(:,2) = [0; 1; 0; 0; 0]; % df/dy
    
    if abs(w) > eps
        dF(:, 3) = [2*sin(w*T/2)*cos(w*T/2 + th)/w;
            2*sin(w*T/2)*sin(w*T/2 + th)/w;
            1;
            0;
            0];     % df/dV
        
        dF(:, 4) = [-2*V*sin(w*T/2)*sin(w*T/2 + th)/w;
            2*V*sin(w*T/2)*cos(w*T/2 + th)/w;
            0;
            1;
            0];     % df/dth
        
        dF(:,5) = [2*V*(w*T*cos(w*T+th)/2 - sin(w*T/2)*cos(w*T/2 + th))/w^2;
            2*V*(w*T*sin(w*T+th)/2 - sin(w*T/2)*sin(w*T/2 + th))/w^2;
            0;
            T;
            1];     % df/dw
    else
        dF(:,3) = [T*cos(th);
            T*sin(th);
            1;
            0;
            0];     % df/dV
        
        dF(:,4) = [-T*V*sin(th);
            T*V*cos(th);
            0;
            1;
            0];     % df/dth
        
        dF(:,5) = [-V*T^2*sin(th)/2;
            V*T^2*cos(th)/2;
            0;
            T;
            1];
    end
end