classdef myExtendedKalmanFilter < handle
    %UNTITLED3 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        F   % Function Handle
        dF  % Function Handle
        H
        P
        Q
        R
        y_hat
        dt
        t
    end
    
    methods
        function obj = myExtendedKalmanFilter(F, dF, H, P, Q, R, y_hat0, dt)
            % Intialize kalman filter
            obj.F = F;
            obj.dF = dF;
            obj.H = H;
            obj.P = P;
            obj.Q = Q;
            obj.R = R;
            obj.y_hat = y_hat0;
            obj.dt = dt;
            obj.t = -dt;
        end
        
        function obj = step(obj)
            obj.y_hat = obj.F(obj.y_hat);
            dF_ = obj.dF(obj.y_hat);
            obj.P = dF_*obj.P*dF_' + obj.Q;
            obj.t = obj.t + obj.dt;
        end
        
        function obj = update(obj, measure)
            r = measure  - obj.H*obj.y_hat;
            k = obj.P*obj.H'/(obj.R + obj.H*obj.P*obj.H');
            obj.y_hat = obj.y_hat + k*r;
            obj.P = (eye(size(obj.P)) - k*obj.H)*obj.P;
        end
    end
end

