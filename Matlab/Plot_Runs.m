range = 10:length(t_measure);

figure()
plot(t_measure(range), TKF.p_RMS(range), 'b')
hold on
plot(t_measure(range), FAB.p_RMS(range), 'r')
plot(t_measure(range), FKF.p_RMS(range), 'k')
% plot(t_measure(range), EKF.p_RMS(range), 'g')
plot(t_measure(range), meas_Error_RMS(range), 'm--')
for i = 1:length(scen.turns)
    xline(scen.turns(i), 'k--', 'DisplayName', "T " + i);
end
legend(["TKF", "FAB", "FKF", "Measure"], 'Location', 'best')
hold on
grid on; grid minor
title("Positional Error RMS")
xlabel("Time (s)")
ylabel("Error RMS")


figure()
plot(t_measure(range), TKF.v_RMS(range), 'b')
hold on
plot(t_measure(range), FAB.v_RMS(range), 'r')
plot(t_measure(range), FKF.v_RMS(range), 'k')
% plot(t_measure(range), EKF.v_RMS(range), 'g')
for i = 1:length(scen.turns)
    xline(scen.turns(i), 'k--');
end
hold on
grid on; grid minor
title("Velocity Error RMS")
xlabel("Time (s)")
ylabel("Error RMS")

figure()
plot(t_measure, FAB.TI)
for i = 1:length(scen.turns)
    xline(scen.turns(i), 'k--');
end
title("Tracking Index Estimate")
xlabel("Time (s)")
ylabel("Tracking Index \Lambda")

figure()
plot(t_measure, FKF.sig_est')
for i = 1:length(scen.turns)
    xline(scen.turns(i), 'k--');
end
title("Process Noise Estimate")
xlabel("Time (s)")
ylabel("Noise Est \omega_p")