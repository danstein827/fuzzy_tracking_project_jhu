PathSetup
clc; clear; % close all

N = 1000;
Scenario2

baseSeed = 87231;

if scen.USE_SIMULINK
    rng(baseSeed+10);
    Seeds = round(99999*rand([1 N]));
    
    for i = 1:length(Seeds)
        paramSet(i) = ...
            Simulink.BlockDiagram.modifyTunableParameters(rsim, 'seedParam', Seeds(i));
    end
    
    disp("Running Simulations")
    
    for i = 1:length(Seeds)
        simout(i) = sim(mdl,'SimulationMode','rapid',...
                      'RapidAcceleratorUpToDateCheck','off', ...
                      'RapidAcceleratorParameterSets',paramSet(i));
        if (mod(i, 10) == 0)
            disp("Sim Number: " + i)
        end
    end
end


fprintf("\n\nTraditional Kalman Filter Sim\n")
Kalman_Monte_Carlo
TKF.p_RMS = pos_Error_RMS;
TKF.v_RMS = vel_Error_RMS;
TKF.p_STD = pos_Error_STD;
TKF.v_STD = vel_Error_RMS;

fprintf("\n\nFuzzy Alpha-Beta Filter\n")
Fuzzy_Monte_Carlo
FAB.p_RMS = pos_Error_RMS;
FAB.v_RMS = vel_Error_RMS;
FAB.p_STD = pos_Error_STD;
FAB.v_STD = vel_Error_RMS;
FAB.TI = tracking_Index_RMS;

fprintf("\n\nFuzzy Kalman Filter\n")
Fuzzy_Kalman_Monte_Carlo
FKF.p_RMS = pos_Error_RMS;
FKF.v_RMS = vel_Error_RMS;
FKF.p_STD = pos_Error_STD;
FKF.v_STD = vel_Error_RMS;
FKF.sig_est = sig_est_RMS;

% fprintf("\n\nExtended Kalman Filter\n")
% EKF_Monte_Carlo
% EKF.p_RMS = pos_Error_RMS;
% EKF.v_RMS = vel_Error_RMS;
% EKF.p_STD = pos_Error_STD;
% EKF.v_STD = vel_Error_RMS;
