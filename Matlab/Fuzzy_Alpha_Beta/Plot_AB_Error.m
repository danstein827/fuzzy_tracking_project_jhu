figure()
subplot(211)
semilogy(meas_hist(:,1), sqrt((meas_hist(:,4) - meas_hist(:,12)).^2 + ...
    (meas_hist(:,6) - meas_hist(:,14)).^2))
title("Positional Error")
subplot(212)
semilogy(meas_hist(:,1), sqrt((meas_hist(:,5) - meas_hist(:,13)).^2 + ...
    (meas_hist(:,7) - meas_hist(:,15)).^2))
title("Velocity Error")