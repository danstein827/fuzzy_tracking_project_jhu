figure(1)
plot(t_measure(10:end), pos_Error_RMS(10:end))

figure(2)
plot(t_measure(10:end), vel_Error_RMS(10:end))

figure(3)
plot(t_measure(10:end), tracking_Index_RMS(10:end))