% Check if seed has been created, for when not running via monte-carlo
if not(exist('seed'))
    seed = 1234632;
end
rng(seed);

% Check if a simulation file as been loaded
if not(exist('scen'))
    Scenario2
end

% Setup time variabled
t = 0:scen.dt:scen.t_end;

% Run target simulation
[t, y] = ode113(scen.target_dyn, t, scen.Y0);

% Initial estimate is blank
y_hat = [0; 0; 0; 0; 0];
n_meas = 0;     % Number of radar measurements taken
next_t_measure = 0; % Time the next measurement should be taken

B = [scen.dt^2/2    0;
    scen.dt         0;
    scen.dt^2/2     0;
    scen.dt         0;
    0               1]; 
Q = blkdiag(zeros(2), scen.dt^2*scen.sig_v^2, [scen.dt^3*scen.sig_v^2/3....
    scen.dt^2*scen.sig_v^2/2; scen.dt^2*scen.sig_v^2/2 scen.dt^2*scen.sig_v^2]);  % B*B'*((scen.sig_v)^2 + 0.1);
R = eye(2)*scen.sig_m^2;
P = 4*eye(5);
H = [1 0 0 0 0;
    0 1 0 0 0];
Nr = 30;
r_buffer = zeros([Nr 2]);
C = 0;

% Initialize history arrays
y_hat_hist = zeros([length(y) 5]);
meas_hist = zeros([0 15]);

for i = 1:length(t)
    % Check if it is time to take a radar measurement
    if t(i) >= next_t_measure
        n_meas = n_meas + 1;    % Increment number of measurements
        next_t_measure = next_t_measure + scen.dt_m; % Schedule next measurement
        % Take a measurement
        measure = scen.H*y(i,:)' + scen.sig_m*randn([2 1]);
        
        % Measure History
        % C 1 - Time
        % C 2-3 - Radar measurement at t
        % C 4-7 - Target actual postion
        % C 8-12 - Target estimate pre-adjustment
        % C 13-17 - Target estimate post-adjustment
        meas_hist(end+1, 1:12) = [t(i) measure' y(i,:) y_hat'];        
        
        % Get the residual and save in buffer
        r = measure - H*y_hat;
        r_buffer(mod(n_meas - 1, Nr)+1,:) = r';
        
        % Calculate C
        C = 0;
        for n = 1:Nr
            C = C + r_buffer(n,:)'*r_buffer(n,:);
        end
        C = C/Nr;
        
%         if n_meas > 5
%             % Update Measure Noise Cov
%             R = C + H*P*H';
%         end
        % Kalman Gain
        k = P*H'/(R + H*P*H');
        
        %if n_meas > Nr
            % Update Process Noise Cov
            Q = k*C*k';
        %end
        
        % Update prediction and covariance
        y_hat = y_hat + k*r;
        P = (eye(5) - k*H)*P;
        
        meas_hist(end, 13:17) = y_hat';
    end        
    
    y_hat_hist(i,:) = y_hat';
    
    dF = getGradF(scen.dt, y_hat);
    y_hat = polarVTurn(y_hat, scen.dt);
    P = dF*P*dF' + Q;
    
end

function dF = getGradF(T,Y)
    y_next = polarVTurn(Y,T);
    
    eps = 0.000001;
    
    for i = 1:5
        e = zeros(size(Y));
        e(i) = eps;
        dF(:,i) = (polarVTurn(Y+e, T) - y_next)/eps;
    end
    
end

