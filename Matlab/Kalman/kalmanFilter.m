classdef kalmanFilter < handle
    %UNTITLED3 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        F
        H
        P
        Q
        R
        y_hat
        dt
        t
    end
    
    methods
        function obj = kalmanFilter(F, H, P, Q, R, y_hat0, dt)
            % Intialize kalman filter
            obj.F = F;
            obj.H = H;
            obj.P = P;
            obj.Q = Q;
            obj.R = R;
            obj.y_hat = y_hat0;
            obj.dt = dt;
            obj.t = -dt;
        end
        
        function kf = step(kf)
            kf.y_hat = kf.F * kf.y_hat;
            kf.P = kf.F*kf.P*kf.F' + kf.Q;
            kf.t = kf.t + kf.dt;
        end
        
        function kf = update(kf, measure)
            r = measure  - kf.H*kf.y_hat;
            k = kf.P*kf.H'/(kf.R + kf.H*kf.P*kf.H');
            kf.y_hat = kf.y_hat + k*r;
            kf.P = (eye(size(kf.P)) - k*kf.H)*kf.P;
        end
    end
end