% Load scenario
if not(exist('scen'))
    Scenario1
end
if not(exist('N'))
    N = 10000;      % Number of trials to run
end

if not(exist('baseSeed'))
    baseSeed = 86524;
end

% Generate a sequence of seeds
rng(baseSeed)
seeds = urandi(100, 99999, N);
t_measure = 0:scen.dt_m:scen.t_end;

pos_Error = zeros([N length(t_measure)]);
vel_Error = zeros([N length(t_measure)]);

for trial = 1:N
    seed = seeds(trial);
    
    if scen.USE_SIMULINK
        y = simout(1, trial).Y;
        t = simout(1, trial).tout;
    end
    
    % Run simulation
    Kalman_Sim;
    
    pos_Error(trial,:) = ((meas_hist(:,4) - meas_hist(:,12)).^2 + ...
        (meas_hist(:,6) - meas_hist(:,14)).^2)';
    vel_Error(trial,:) = ((meas_hist(:,5) - meas_hist(:,13)).^2 + ...
        (meas_hist(:,7) - meas_hist(:,15)).^2)';
    
    if (mod(trial, 500) == 0)
        disp("Trial Number: " + trial)
    end
end

pos_Error_RMS = sqrt(sum(pos_Error/N, 1));
vel_Error_RMS = sqrt(sum(vel_Error/N, 1));

pos_Error_STD = (sum(pos_Error - pos_Error_RMS, 1)/(N-1)).^2;
vel_Error_STD = (sum(vel_Error - vel_Error_RMS, 1)/(N-1)).^2;