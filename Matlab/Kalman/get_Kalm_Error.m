% Gets the error from a kalman simulation
KAL.p_error = (sum((meas_hist(:, [4 6]) - meas_hist(:, [12 14])).^2, 2));
KAL.v_error = (sum((meas_hist(:, [5 7]) - meas_hist(:, [13 15])).^2, 2));
KAL.m_error = (sum((meas_hist(:, [4 6]) - meas_hist(:, [2 3])).^2, 2));
KAL.J1 = sqrt(sum(KAL.m_error/(length(KAL.m_error) - 1)));
KAL.J2 = [sqrt(sum(KAL.p_error/(length(KAL.p_error) - 1)));
    sqrt(sum(KAL.v_error/(length(KAL.v_error) - 1)))];