% Check if seed has been created, for when not running via monte-carlo
if not(exist('seed'))
    seed = 1234632;
end
rng(seed);

% Check if a simulation file as been loaded
if not(exist('scen'))
    Scenario1
end

% Setup time variable
t = 0:scen.dt:scen.t_end;

% Run target simulation
if not(exist('y'))
    [t, y] = ode113(scen.target_dyn, t, scen.Y0);
end

% Initial estimate is blank
y_hat = [0; 0; 0; 0];
n_meas = 0;     % Number of radar measurements taken
next_t_measure = 0; % Time the next measurement should be taken
Q = scen.G*diag((scen.sig_v+scen.sig_v_rand).^2)*scen.G';
R = eye(2)*scen.sig_m^2;
P = 4*eye(4);

kf = kalmanFilter(scen.F, scen.H, P, Q, R, [0; 0; 0; 0], scen.dt);

% Initialize history arrays
y_hat_hist = zeros(size(y));
meas_hist = zeros([0 15]);

for i = 1:length(t)
    % Check if it is time to take a radar measurement
    if t(i) >= next_t_measure
        n_meas = n_meas + 1;    % Increment number of measurements
        next_t_measure = next_t_measure + scen.dt_m; % Schedule next measurement
        % Take a measurement
        measure = scen.H*y(i,:)' + scen.sig_m*randn([2 1]);
        
        % Measure History
        % C 1 - Time
        % C 2-3 - Radar measurement at t
        % C 4-7 - Target actual postion
        % C 8-11 - Target estimate pre-adjustment
        % C 12-15 - Target estimate post-adjustment
        meas_hist(end+1, 1:11) = [t(i) measure' y(i,:) kf.y_hat'];        
        kf.update(measure);
        meas_hist(end, 12:15) = kf.y_hat';
    end        
    
    y_hat_hist(i,:) = kf.y_hat';
    
    kf.step();
    
end