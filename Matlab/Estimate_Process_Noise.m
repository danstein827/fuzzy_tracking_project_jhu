% Estimates process noise varience due to turns
% Setup time variable
t = 0:scen.dt:scen.t_end;

% Run target simulation
if not(exist('y'))
    [t, y] = ode113(scen.target_dyn, t, scen.Y0);
end

u = zeros([2 length(t)]);

for i = 1:length(t)
    u(:,i) = scen.accel([t(i) y(i,:)]);
end

sig_v = std(u, [], 2);