%% Stores variables and constants to define scenario
scen.name = "Scenario Random";
scen.t_end = 800;    % How long to run scenario
scen.dt = 1;         % Scenario run frequency
scen.t = 0:scen.dt:scen.t_end;
scen.USE_SIMULINK = 1;

%% Define target parameters
% Target state = x pos, x speed, y pos, y speed
scen.Y0 = [120; -0.3867; 60; -0.3867];    % Initial target state
% Target dynamic function
scen.target_dyn = @(t, y) target_dynamics(t, y, accel_profile(t, y));
scen.u_lookupTable = zeros([2 length(scen.t)]);
scen.u_lookupTable(1,:) = arrayfun(@(t) accel_seq(t), scen.t);
scen.u_lookupTable(2,:) = arrayfun(@(t) turn_seq(t), scen.t);
scen.sig_v = .1;     % Process noise (target accel)
scen.sig_v_rand = [.01; 0.2];
scen.turns = [];

if scen.USE_SIMULINK
    seedParam = Simulink.Parameter;
    seedParam.CoderInfo.StorageClass = 'SimulinkGlobal';
    seedParam.Value = 823120;
    w_lookupTable = Simulink.Parameter;
    w_lookupTable.CoderInfo.StorageClass = 'SimulinkGlobal';
    w_lookupTable.Value = scen.u_lookupTable(1,:);
    a_lookupTable = Simulink.Parameter;
    a_lookupTable.CoderInfo.StorageClass = 'SimulinkGlobal';
    a_lookupTable.Value = scen.u_lookupTable(2,:);
    mdl = 'Target_Sim_2019a';
    rsim = Simulink.BlockDiagram.buildRapidAcceleratorTarget(mdl);
end

%% Define Radar Parameters
scen.dt_m = 5;   % Radar update rate
scen.sig_m = .1;     % Radar measurement noise
scen.H = [1 0 0 0; 0 0 1 0];    % Radar measurement matrix
scen.F = [1 scen.dt 0 0;
          0 1 0 0;
          0 0 1 scen.dt;
          0 0 0 1]; % Constant velocity interpolation between updates
scen.Fm = [1    scen.dt_m   0       0;
           0    1           0       0;
           0    0           1       scen.dt_m;
           0    0           0       1]; % Measurement interpolation
scen.G = [0.5*scen.dt^2 0;
          scen.dt       0
          0             0.5*scen.dt^2;
          0             scen.dt]; % Control (acceleration) matrix
scen.K0 = [1 0;
            0 0;
            0 1;
            0 0];
scen.K1 = [1 0;
            1/scen.dt 0;
            0 1;
            0 1/scen.dt];
scen.TI = (scen.sig_v/scen.sig_m)*scen.dt_m^2;   % Tracking index for constant filter gain
scen.alpha = (-1/8)*(scen.TI^2 + 8*scen.TI - ...
    (scen.TI + 4) * sqrt(scen.TI^2 + 8*scen.TI));   % Alpha for constant filter gain
scen.beta = 2*(2 - scen.alpha) - 4*sqrt(1 - scen.alpha);    % Beta for constant filter gain
scen.K = [scen.alpha 0;
          scen.beta/scen.dt 0;
          0 scen.alpha;
          0 scen.beta/scen.dt];
scen.turn = @(t) turn_seq(t);
scen.accel = @(x) accel_profile(x(1), [x(2); x(3); x(4); x(5)]);
        
%% Target Function definitions
% Define target turn sequence
function w = turn_seq(t)
    w = 0;
end

function a = accel_seq(t)
    a = 0;
end

% Convert turn sequence to normal acceleration
function u = accel_profile(t, y)
    u = [0; 0];

%     persistent T U1 U2
%     
%     if isempty(T) || (t == 0)
%         T = 0;
%         U1 = 0.1*randn();
%         U2 = 0.1*randn();
%         
%         u = [U1; U2];
%         return
%     end
%     
%     if t > T(end)
%         T(end+1) = t;
%         U1(end+1) = 0.1*randn();
%         U2(end+1) = 0.1*randn();
%     end
% 
%     u = [interp1(T', U1', t, 'previous'); interp1(T', U1', t, 'previous')];
end


