% Uniform random integer
% Generate N uniformly distributed random integers between low and high

function rv = urandi(low, high, N)
    if (low >= high)
        rv = ones([1 N])*low;
    else
        rv = floor(rand([1 N])*(high-low+1) + low);
        
        rv(rv > high) = high;
    end
end